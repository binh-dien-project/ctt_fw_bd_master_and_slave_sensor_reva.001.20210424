#ifndef __INCLUDE_H
#define __INCLUDE_H

#define _USE_MATH_DEFINES

#include "main.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "config.h"
#include "LCD4/lcd4.h"
#include "DS3231/ds3231.h"
#include "BUTTON/button.h"
#include "FLASH/flash.h"
#include "MODE/mode.h"
#include "WIFI/wifi.h"
#include "SENSOR/sensor.h"
#include "CM_RF/cm.h"


#endif
