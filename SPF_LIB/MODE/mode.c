/*
 * mode.c
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#include "../include.h"

#define TIMERDEBOUNCEMAX 100 //Chong rung phim
#define TIMERCHECKLONGCLICK 1000
#define TIMERHANDLELONGCLICK 50

/*
 * extern test for master
 */
extern uint16_t nTimePeriod;
extern uint16_t nTimeWifiCheck;
extern uint16_t test;
extern CMState_t CMState;

/* extern */
extern _DS3231 tRtc;
extern _WIFI WifiInfo;
extern _SENSOR_IRRI tSenIrriVal;

_DEV_INFO tDevInfo;

/* local */
int16_t nTimeRefreshLcd;
uint16_t nTimerCheckDebounce;
bool refreshLcd;
bool success;
char ac_BuffLcd[30];
float fWaterTempStandard;

errCode_t errCode;
flagMode_t flagMode;
modeDisplay_t modeDisplay;
modeSetting_t modeSetting;
settingEthernet_t settingEthernet;
settingThreshold_t settingThreshold;
settingRtc_t settingRtc;
settingEXIO_t settingEXIO;
settingManual_t settingManual;
settingCalibEC_t settingCalibEC;
settingCalibWT_t settingCalibWT;
settingCalibPH_t settingCalibPH;
settingCalibDO_t settingCalibDO;
settingCalibORP_t settingCalibORP;
settingCalibTE_t settingCalibTE;
settingCalibHU_t settingCalibHU;
settingCalibEcSoil_t settingCalibEcSoil;
settingCalibPhSoil_t settingCalibPhSoil;
settingCalibHuSoil_t settingCalibHuSoil;
settingRf_t settingRf;
settingInfo_t settingInfo;

/* static */
static void MODE_SLAVE_HandleButtonMode(void);
static void MODE_SLAVE_HandleButtonSet(void);
static void MODE_SLAVE_HandleButtonUp(void);
static void MODE_SLAVE_HandleButtonDown(void);
static void MODE_RefreshLcd(void);
static void MODE_Disp_FarmName(void);
static void MODE_Disp_RTC(void);
static void MODE_Disp_WifiWave(void);
static void MODE_Disp_Battery(void);
static void MODE_Disp_Success(bool ret);
static void MODE_Disp_PH_EC_WT_Dual(void);
static void MODE_Disp_DO_ORP_Dual(void);
static void MODE_Disp_Vol_Cur(void);
static void MODE_Disp_Setting_ASTERISK(uint8_t pos);
static void MODE_SLAVE_Disp_Setting_Page1(void);
static void MODE_SLAVE_Disp_Setting_Page2(void);
static void MODE_SLAVE_Disp_Setting_Page3(void);
static void MODE_SLAVE_Disp_Setting_Page4(void);
static void MODE_MASTER_Disp_Setting_Page1(void);
static void MODE_MASTER_Disp_Setting_Page2(void);
static void MODE_MASTER_HandleButtonMode(void);
static void MODE_MASTER_HandleButtonSet(void);
static void MODE_MASTER_HandleButtonUp(void);
static void MODE_MASTER_HandleButtonDown(void);

void MODE_SLAVE_Init(void)
{
	errCode = ERR_NONE;
	MY_FLASH_SetSectorAddrs(11, EEPROM_START_ADDRESS);
	MODE_SLAVE_LoadParams();
	flagMode = MODE_DISPLAY;
	modeDisplay = DISPLAY_EC_PH_WT;
	modeSetting = SETTING_RF;
	settingRf = SETTING_RF_NONE;
	settingThreshold = SETTING_THRESHOLD_NONE;
	settingRtc = SETTING_RTC_NONE;
	settingEXIO = SETTING_EXIO_NONE;
	settingManual = SETTING_MANUAL_NONE;
	settingCalibEC = SETTING_CALIB_EC_NONE;
	settingCalibWT = SETTING_CALIB_WT_NONE;
	settingCalibPH = SETTING_CALIB_PH_NONE;
	settingCalibDO = SETTING_CALIB_DO_NONE;
	settingCalibORP = SETTING_CALIB_ORP_NODE;
	settingInfo = SETTING_INFO_NONE;
	LCD4_Clear();
}

void MODE_MASTER_Init(void)
{
	errCode = ERR_NONE;
	MY_FLASH_SetSectorAddrs(11, EEPROM_START_ADDRESS);
	MODE_MASTER_LoadParams();
	flagMode = MODE_DISPLAY;
	modeDisplay = DISPLAY_MASTER;
	modeSetting = SETTING_RF;
	settingRf = SETTING_RF_NONE;
	settingEthernet = SETTING_ETHERNET_NONE;
	settingRtc = SETTING_RTC_NONE;
	settingInfo = SETTING_INFO_NONE;
}

void MODE_Disp_Success(bool ret)
{
	flagMode = MODE_CONFIG_SUCCESS;
	success = ret;
}

void MODE_RefreshLcd(void)
{
	if (refreshLcd == true)
	{
		LCD4_Clear();
		refreshLcd = false;
	}
}

void MODE_Disp_Error(void)
{
	if (CMState == CM_ERROR_CONFIG)
		errCode = ERR_CM_CONFIG_FAIL;
	else if (WifiInfo.bState == false)
		errCode = ERR_WIFI_DISCONNECTED;
	else
		errCode = ERR_NONE;

	switch (errCode)
	{
	case ERR_NONE:
		LCD4_WriteCursor(0, 17, (int8_t *)"   ");
		break;
	case ERR_CM_CONFIG_FAIL:
		if (nTimeRefreshLcd > 600)
			LCD4_WriteCursor(0, 17, (int8_t *)"E01");
		else
			LCD4_WriteCursor(0, 17, (int8_t *)"   ");
		break;
	case ERR_WIFI_DISCONNECTED:
		if (nTimeRefreshLcd > 600)
			LCD4_WriteCursor(0, 17, (int8_t *)"E11");
		else
			LCD4_WriteCursor(0, 17, (int8_t *)"   ");
		break;
	}
}

void MODE_Disp_FarmName(void)
{
#ifdef SPF_SLAVE
	sprintf(ac_BuffLcd, "S%03d", tDevInfo.nSlaveCode);
	LCD4_WriteCursor(0, 6, (int8_t *)ac_BuffLcd);
#else
	sprintf(ac_BuffLcd, "TOM%03d%03d", (int)tDevInfo.nMasterCode, (int)1);
	LCD4_WriteCursor(0, 6, (int8_t *)ac_BuffLcd);
#endif
}

void MODE_Disp_RTC(void)
{
	sprintf(ac_BuffLcd, "%02d:%02d", tRtc.Hour, tRtc.Min);
	LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
}

void MODE_Disp_WifiWave(void)
{
	LCD4_WriteCursor(0, 16, (int8_t *)"");
	LCD4_WriteCommand(1, 0);
	WifiInfo.nRssi = 60;
	if (WifiInfo.nRssi > 77)
	{
		LCD4_WriteCommand(1, 1);
	}
	else if ((WifiInfo.nRssi <= 77) && (WifiInfo.nRssi > 72))
	{
		LCD4_WriteCommand(1, 2);
	}
	else if ((WifiInfo.nRssi <= 72) && (WifiInfo.nRssi > 67))
	{
		LCD4_WriteCommand(1, 2);
		LCD4_WriteCommand(1, 3);
	}
	else
	{
		LCD4_WriteCommand(1, 2);
		LCD4_WriteCommand(1, 3);
		LCD4_WriteCommand(1, 4);
	}
}

void MODE_Disp_Battery(void)
{
	LCD4_WriteCursor(0, 11, (int8_t *)"");
	LCD4_WriteCommand(1, 5);
	LCD4_WriteCursor(0, 12, (int8_t *)"100%");
}

void MODE_Disp_PH_EC_WT_Dual(void)
{
	sprintf(ac_BuffLcd, "PH: %1.2f  %1.2f", (float)tSenIrriVal.dPh1, (float)tSenIrriVal.dPh2);
	LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
	sprintf(ac_BuffLcd, "EC: %1.2f  %1.2f mS/cm", (float)tSenIrriVal.dEc1, (float)tSenIrriVal.dEc2);
	LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
	sprintf(ac_BuffLcd, "WT: %2.2f %2.2f ", (float)tSenIrriVal.dWT1, (float)tSenIrriVal.dWT2);
	LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
	LCD4_WriteCommand(1, 223);
	LCD4_WriteCommand(1, 67);
}

void MODE_Disp_DO_ORP_Dual(void)
{
	sprintf(ac_BuffLcd, "DO:   %2.1f  %2.1f mg/L", (float)(tSenIrriVal.dDo1 / 1000), (float)(tSenIrriVal.dDo2 / 1000));
	LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
	sprintf(ac_BuffLcd, "ORP:  %4.0f  %4.0f mV", (float)tSenIrriVal.dOrp1, (float)tSenIrriVal.dOrp2);
	LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
}

void MODE_Disp_Vol_Cur(void)
{
	// sprintf(ac_BuffLcd, "Cur: %1.2f  %1.2f  (A)", fabs(tSenIrriVal.dBat1mAmpe / 1000), fabs(tSenIrriVal.dSol1mAmpe / 1000));
	// LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
	// sprintf(ac_BuffLcd, "Vol: %2.1f  %2.1f  (V)", (double)(tSenIrriVal.dBat1mv / 1000), (double)(tSenIrriVal.dSol1mv / 1000));
	// LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);

	sprintf(ac_BuffLcd, "Batte: %2.1fV  %1.2fA", (double)(tSenIrriVal.dBat1mv / 1000), fabs(tSenIrriVal.dBat1mAmpe / 1000));
	LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
	sprintf(ac_BuffLcd, "Solar: %2.1fV  %1.2fA", (double)(tSenIrriVal.dSol1mv / 1000), fabs(tSenIrriVal.dSol1mAmpe / 1000));
	LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
}

void MODE_Disp_Setting_ASTERISK(uint8_t pos)
{
	switch (pos)
	{
	case 0:
		LCD4_WriteCursor(0, 19, (int8_t *)"*");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case 1:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)"*");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case 2:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)"*");
		LCD4_WriteCursor(3, 19, (int8_t *)" ");
		break;
	case 3:
		LCD4_WriteCursor(0, 19, (int8_t *)" ");
		LCD4_WriteCursor(1, 19, (int8_t *)" ");
		LCD4_WriteCursor(2, 19, (int8_t *)" ");
		LCD4_WriteCursor(3, 19, (int8_t *)"*");
		break;
	default:
		break;
	}
}

void MODE_SLAVE_Disp_Setting_Page1(void) // RF,THRESHOLD,RTC
{
	MODE_RefreshLcd();
	LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
	LCD4_WriteCursor(1, 0, (int8_t *)"1. RF");
	LCD4_WriteCursor(2, 0, (int8_t *)"2. Threshold");
	LCD4_WriteCursor(3, 0, (int8_t *)"3. Clock");
}

void MODE_SLAVE_Disp_Setting_Page2(void) // EXIO, MANUAL, CALIB EC
{
	MODE_RefreshLcd();
	LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
	LCD4_WriteCursor(1, 0, (int8_t *)"4. External IO");
	LCD4_WriteCursor(2, 0, (int8_t *)"5. Manual Control");
	LCD4_WriteCursor(3, 0, (int8_t *)"6. Calib Ec");
}

void MODE_SLAVE_Disp_Setting_Page3(void) // CALIB WT, CALIB PH, CALIB DO
{
	MODE_RefreshLcd();
	LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
	LCD4_WriteCursor(1, 0, (int8_t *)"7. Calib Wt");
	LCD4_WriteCursor(2, 0, (int8_t *)"8. Calib Ph");
	LCD4_WriteCursor(3, 0, (int8_t *)"9. Calib Do");
}

void MODE_SLAVE_Disp_Setting_Page4(void) // CALIB ORP, DEFAULT, INFO
{
	MODE_RefreshLcd();
	LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
	LCD4_WriteCursor(1, 0, (int8_t *)"10. Calib Orp");
	LCD4_WriteCursor(2, 0, (int8_t *)"11. Factory Reset");
	LCD4_WriteCursor(3, 0, (int8_t *)"12. Device Info");
}

void MODE_MASTER_Disp_Setting_Page1(void)
{
	MODE_RefreshLcd();
	LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
	LCD4_WriteCursor(1, 0, (int8_t *)"1. RF");
	LCD4_WriteCursor(2, 0, (int8_t *)"2. Wifi");
	LCD4_WriteCursor(3, 0, (int8_t *)"3. Clock");
}

void MODE_MASTER_Disp_Setting_Page2(void)
{
	MODE_RefreshLcd();
	LCD4_WriteCursor(0, 0, (int8_t *)"Setting");
	LCD4_WriteCursor(1, 0, (int8_t *)"4. Factory Reset");
	LCD4_WriteCursor(2, 0, (int8_t *)"5. Device Info");
}

void MODE_MASTER_Lcd(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		MODE_RefreshLcd();
		MODE_Disp_FarmName();
		MODE_Disp_RTC();
		// MODE_Disp_WifiWave();
		MODE_Disp_Error();

		sprintf(ac_BuffLcd, "Period: %03d", nTimePeriod);
		LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
		sprintf(ac_BuffLcd, "Check:  %02d", nTimeWifiCheck);
		LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
		sprintf(ac_BuffLcd, "Test:  %04d", test);
		LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
		break;
	case MODE_SETTING:
		switch (modeSetting)
		{
		case SETTING_RF:
			switch (settingRf)
			{
			case SETTING_RF_NONE:
				MODE_MASTER_Disp_Setting_Page1();
				MODE_Disp_Setting_ASTERISK(1);
				break;
			default:
				MODE_RefreshLcd();
				sprintf(ac_BuffLcd, "Self ID:    %04d", tDevInfo.nCM_SID);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "Dest ID:    %04d", tDevInfo.nCM_DID);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "PAN ID:     %04d", tDevInfo.nCM_PID);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "Channel ID: %04d", tDevInfo.nCM_CID);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			}
			switch (settingRf)
			{
			case SETTING_RF_SID:
				MODE_Disp_Setting_ASTERISK(0);
				break;
			case SETTING_RF_DID:
				MODE_Disp_Setting_ASTERISK(1);
				break;
			case SETTING_RF_PID:
				MODE_Disp_Setting_ASTERISK(2);
				break;
			case SETTING_RF_CID:
				MODE_Disp_Setting_ASTERISK(3);
				break;
			default:
				break;
			}
			break;
		case SETTING_INTERNET:
			switch (settingEthernet)
			{
			case SETTING_ETHERNET_NONE:
				MODE_MASTER_Disp_Setting_Page1();
				MODE_Disp_Setting_ASTERISK(2);
				break;
			default:
				MODE_RefreshLcd();
				sprintf(ac_BuffLcd, "IP: %s", WifiInfo.nIpAddress);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "RSSI: %02d", WifiInfo.nRssi);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				if (WifiInfo.bState == true)
				{
					LCD4_WriteCursor(1, 10, (int8_t *)"Connected");
				}
				else
				{
					LCD4_WriteCursor(1, 10, (int8_t *)"Lost     ");
				}
				sprintf(ac_BuffLcd, "T Upload:  %03d(s)", tDevInfo.nTimeUpload);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "<<     RESET    >>");
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			}
			switch (settingEthernet)
			{
			case SETTING_ETHERNET_RESET:
				MODE_Disp_Setting_ASTERISK(3);
				break;
			case SETTING_ETHERNET_TIME_UPLOAD:
				MODE_Disp_Setting_ASTERISK(2);
				break;
			default:
				break;
			}
			break;
		case SETTING_RTC:
			switch (settingRtc)
			{
			case SETTING_RTC_NONE:
				MODE_MASTER_Disp_Setting_Page1();
				MODE_Disp_Setting_ASTERISK(3);
				break;
			default:
				MODE_RefreshLcd();
				sprintf(ac_BuffLcd, "Hour:");
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "Minute");
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				break;
			}
			switch (settingRtc)
			{
			case SETTING_RTC_HOUR:
				MODE_Disp_Setting_ASTERISK(0);
				break;
			case SETTING_RTC_MINUTE:
				MODE_Disp_Setting_ASTERISK(1);
				break;
			default:
				break;
			}
			break;
		case SETTING_DEFAULT:
			MODE_MASTER_Disp_Setting_Page2();
			MODE_Disp_Setting_ASTERISK(1);
			break;
		case SETTING_INFO:
			switch (settingInfo)
			{
			case SETTING_INFO_NONE:
				MODE_MASTER_Disp_Setting_Page2();
				MODE_Disp_Setting_ASTERISK(2);
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
		break;
	case MODE_CONFIG_SUCCESS:
		LCD4_Clear();
		HAL_Delay(1);
		if (success == true)
			LCD4_WriteCursor(2, 7, (int8_t *)"Saved!");
		else
			LCD4_WriteCursor(2, 7, (int8_t *)"Failed!");
		HAL_Delay(1000);

		LCD4_Clear();
		HAL_Delay(1);

		flagMode = MODE_SETTING;
		settingRf = SETTING_RF_NONE;
		settingEthernet = SETTING_ETHERNET_NONE;
		settingRtc = SETTING_RTC_NONE;
		settingInfo = SETTING_INFO_NONE;
		break;
	}
}

void MODE_SLAVE_Lcd(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		switch (modeDisplay)
		{
		case DISPLAY_EC_PH_WT:
			MODE_RefreshLcd();
			MODE_Disp_FarmName();
			MODE_Disp_RTC();
			MODE_Disp_Battery();
			MODE_Disp_PH_EC_WT_Dual();
			break;
		case DISPLAY_DO_ORP:
			MODE_RefreshLcd();
			//			MODE_Disp_FarmName();
			//			MODE_Disp_RTC();
			//			MODE_Disp_Battery();
			MODE_Disp_DO_ORP_Dual();
			MODE_Disp_Vol_Cur();
			break;
		default:
			break;
		}
		break;
	case MODE_SETTING:
		switch (modeSetting)
		{
		case SETTING_RF:
			switch (settingRf)
			{
			case SETTING_RF_NONE:
				MODE_SLAVE_Disp_Setting_Page1();
				MODE_Disp_Setting_ASTERISK(1);
				break;
			case SETTING_RF_SID:
			case SETTING_RF_DID:
			case SETTING_RF_PID:
			case SETTING_RF_CID:
				MODE_RefreshLcd();
				sprintf(ac_BuffLcd, "Self ID:    %04d", tDevInfo.nCM_SID);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "Dest ID:    %04d", tDevInfo.nCM_DID);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "PAN ID:     %04d", tDevInfo.nCM_PID);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "Channel ID: %04d", tDevInfo.nCM_CID);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			case SETTING_RF_TIME_UPLOAD:
				MODE_RefreshLcd();
				sprintf(ac_BuffLcd, "T Upload:   %4d", tDevInfo.nTimeTransmitSensor);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				break;
			}
			switch (settingRf)
			{
			case SETTING_RF_SID:
				MODE_Disp_Setting_ASTERISK(0);
				break;
			case SETTING_RF_DID:
				MODE_Disp_Setting_ASTERISK(1);
				break;
			case SETTING_RF_PID:
				MODE_Disp_Setting_ASTERISK(2);
				break;
			case SETTING_RF_CID:
				MODE_Disp_Setting_ASTERISK(3);
				break;
			case SETTING_RF_TIME_UPLOAD:
				MODE_Disp_Setting_ASTERISK(0);
				break;
			default:
				break;
			}
			break;
		case SETTING_THRESHOLD:
			switch (settingThreshold)
			{
			case SETTING_THRESHOLD_NONE:
				MODE_SLAVE_Disp_Setting_Page1();
				MODE_Disp_Setting_ASTERISK(2);
				break;
			case SETTING_THRESHOLD_EC:
			case SETTING_THRESHOLD_ECLOW:
			case SETTING_THRESHOLD_ECHIGH:
			case SETTING_THRESHOLD_ECDUAL:
				MODE_RefreshLcd();
				sprintf(ac_BuffLcd, "EC:      %1.2f", (float)tDevInfo.nThrehold_Ec / 100);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "EC LOW:  %1.2f", (float)tDevInfo.nThrehold_EcLow / 100);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "EC HIGH: %1.2f", (float)tDevInfo.nThrehold_ECHigh / 100);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "EC DUAL: %1.2f", (float)tDevInfo.nThrehold_ECDual / 100);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			case SETTING_THRESHOLD_WT:
			case SETTING_THRESHOLD_WTLOW:
			case SETTING_THRESHOLD_WTHIGH:
			case SETTING_THRESHOLD_WTDUAL:
				MODE_RefreshLcd();
				sprintf(ac_BuffLcd, "WT:      %2.1f", (float)tDevInfo.nThrehold_WT / 10);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "WT LOW:  %2.1f", (float)tDevInfo.nThrehold_WTLow / 10);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "WT HIGH: %2.1f", (float)tDevInfo.nThrehold_WTHigh / 10);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "WT DUAL: %2.1f", (float)tDevInfo.nThrehold_WTDual / 10);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			case SETTING_THRESHOLD_PH:
			case SETTING_THRESHOLD_PHLOW:
			case SETTING_THRESHOLD_PHHIGH:
			case SETTING_THRESHOLD_PHDUAL:
				MODE_RefreshLcd();
				sprintf(ac_BuffLcd, "PH:      %1.2f", (float)tDevInfo.nThrehold_PH / 100);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "PH LOW:  %1.2f", (float)tDevInfo.nThrehold_PHLow / 100);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "PH HIGH: %1.2f", (float)tDevInfo.nThrehold_PHHigh / 100);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "PH DUAL: %1.2f", (float)tDevInfo.nThrehold_PHDual / 100);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			case SETTING_THRESHOLD_DO:
			case SETTING_THRESHOLD_DOLOW:
			case SETTING_THRESHOLD_DOHIGH:
			case SETTING_THRESHOLD_DODUAL:
				MODE_RefreshLcd();
				sprintf(ac_BuffLcd, "DO: %2.2f", (float)tDevInfo.nThrehold_Do);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "DO: %2.2f", (float)tDevInfo.nThrehold_DoLow);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "DO: %2.2f", (float)tDevInfo.nThrehold_DoHigh);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "DO: %2.2f", (float)tDevInfo.nThrehold_DoDual);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			case SETTING_THRESHOLD_ORP:
			case SETTING_THRESHOLD_ORPLOW:
			case SETTING_THRESHOLD_ORPHIGH:
			case SETTING_THRESHOLD_ORPDUAL:
				MODE_RefreshLcd();
				sprintf(ac_BuffLcd, "ORP: %4f", (float)tDevInfo.nThrehold_Do);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "ORP: %4f", (float)tDevInfo.nThrehold_DoLow);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "ORP: %4f", (float)tDevInfo.nThrehold_DoHigh);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "ORP: %4f", (float)tDevInfo.nThrehold_DoDual);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			default:
				break;
			}
			switch (settingThreshold)
			{
			case SETTING_THRESHOLD_EC:
				MODE_Disp_Setting_ASTERISK(0);
				break;
			case SETTING_THRESHOLD_ECLOW:
				MODE_Disp_Setting_ASTERISK(1);
				break;
			case SETTING_THRESHOLD_ECHIGH:
				MODE_Disp_Setting_ASTERISK(2);
				break;
			case SETTING_THRESHOLD_ECDUAL:
				MODE_Disp_Setting_ASTERISK(3);
				break;
			case SETTING_THRESHOLD_WT:
				MODE_Disp_Setting_ASTERISK(0);
				break;
			case SETTING_THRESHOLD_WTLOW:
				MODE_Disp_Setting_ASTERISK(1);
				break;
			case SETTING_THRESHOLD_WTHIGH:
				MODE_Disp_Setting_ASTERISK(2);
				break;
			case SETTING_THRESHOLD_WTDUAL:
				MODE_Disp_Setting_ASTERISK(3);
				break;
			case SETTING_THRESHOLD_PH:
				MODE_Disp_Setting_ASTERISK(0);
				break;
			case SETTING_THRESHOLD_PHLOW:
				MODE_Disp_Setting_ASTERISK(1);
				break;
			case SETTING_THRESHOLD_PHHIGH:
				MODE_Disp_Setting_ASTERISK(2);
				break;
			case SETTING_THRESHOLD_PHDUAL:
				MODE_Disp_Setting_ASTERISK(3);
				break;
			case SETTING_THRESHOLD_DO:
				MODE_Disp_Setting_ASTERISK(0);
				break;
			case SETTING_THRESHOLD_DOLOW:
				MODE_Disp_Setting_ASTERISK(1);
				break;
			case SETTING_THRESHOLD_DOHIGH:
				MODE_Disp_Setting_ASTERISK(2);
				break;
			case SETTING_THRESHOLD_DODUAL:
				MODE_Disp_Setting_ASTERISK(3);
				break;
			case SETTING_THRESHOLD_ORP:
				MODE_Disp_Setting_ASTERISK(0);
				break;
			case SETTING_THRESHOLD_ORPLOW:
				MODE_Disp_Setting_ASTERISK(1);
				break;
			case SETTING_THRESHOLD_ORPHIGH:
				MODE_Disp_Setting_ASTERISK(2);
				break;
			case SETTING_THRESHOLD_ORPDUAL:
				MODE_Disp_Setting_ASTERISK(3);
				break;
			default:
				break;
			}
			break;
		case SETTING_RTC:
			switch (settingRtc)
			{
			case SETTING_RTC_NONE:
				MODE_SLAVE_Disp_Setting_Page1();
				MODE_Disp_Setting_ASTERISK(3);
				break;
			default:
				MODE_RefreshLcd();
				sprintf(ac_BuffLcd, "Hour: %2d", tRtc.Hour);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "Minute: %2d", tRtc.Min);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				break;
			}
			switch (settingRtc)
			{
			case SETTING_RTC_HOUR:
				MODE_Disp_Setting_ASTERISK(0);
				break;
			case SETTING_RTC_MINUTE:
				MODE_Disp_Setting_ASTERISK(1);
				break;
			default:
				break;
			}
			break;
		case SETTING_EXIO:
			switch (settingEXIO)
			{
			case SETTING_EXIO_NONE:
				MODE_SLAVE_Disp_Setting_Page2();
				MODE_Disp_Setting_ASTERISK(1);
				break;
			default:
				LCD4_WriteCursor(0, 0, (int8_t *)"TBU");
				break;
			}
			break;
		case SETTING_MANUAL:
			switch (settingManual)
			{
			case SETTING_MANUAL_NONE:
				MODE_SLAVE_Disp_Setting_Page2();
				MODE_Disp_Setting_ASTERISK(2);
				break;
			default:
				LCD4_WriteCursor(0, 0, (int8_t *)"TBU");
				break;
			}
			break;
		case SETTING_CALIB_EC:
			switch (settingCalibEC)
			{
			case SETTING_CALIB_EC_NONE:
				MODE_SLAVE_Disp_Setting_Page2();
				MODE_Disp_Setting_ASTERISK(3);
				break;
			case SETTING_CALIB_EC_STANDARD:
			case SETTING_CALIB_CLICK_CALIB_EC:
				MODE_RefreshLcd();
				LCD4_WriteCursor(0, 0, (int8_t *)"Put the probe into");
				LCD4_WriteCursor(1, 0, (int8_t *)"the EC solution");
				sprintf(ac_BuffLcd, "EC sol: %4d mS/cm", tDevInfo.nStandard_Ec);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(3, 0, (int8_t *)"<< CALIBRATION >>");
				break;
			case SETTING_CALIB_EC1_A:
			case SETTING_CALIB_EC2_A:
				MODE_RefreshLcd();
				sprintf(ac_BuffLcd, "Ec1: %2.2f-> A:%4d", tSenIrriVal.dEc1, tDevInfo.nCalib_Ec1A);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "%4.0f mV   > B:%4d", tSenIrriVal.dEc1mv, tDevInfo.nCalib_Ec1B);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "Ec2: %2.2f-> A:%4d", tSenIrriVal.dEc2, tDevInfo.nCalib_Ec2A);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "%4.0f mV   > B:%4d", tSenIrriVal.dEc2mv, tDevInfo.nCalib_Ec2B);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			default:
				break;
			}
			switch (settingCalibEC)
			{
			case SETTING_CALIB_EC_STANDARD:
				MODE_Disp_Setting_ASTERISK(2);
				break;
			case SETTING_CALIB_CLICK_CALIB_EC:
				MODE_Disp_Setting_ASTERISK(3);
				break;
			case SETTING_CALIB_EC1_A:
				MODE_Disp_Setting_ASTERISK(0);
				break;
			case SETTING_CALIB_EC2_A:
				MODE_Disp_Setting_ASTERISK(2);
				break;
			}
			break;
		case SETTING_CALIB_WT:
			switch (settingCalibWT)
			{
			case SETTING_CALIB_WT_NONE:
				MODE_SLAVE_Disp_Setting_Page3();
				MODE_Disp_Setting_ASTERISK(1);
				break;
			default:
				MODE_RefreshLcd();
				LCD4_WriteCursor(0, 0, (int8_t *)"Put the probe into");
				LCD4_WriteCursor(1, 0, (int8_t *)"the Water Temp Sol");
				sprintf(ac_BuffLcd, "WT Stand: %2.2f ", fWaterTempStandard);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				LCD4_WriteCommand(1, 223);
				LCD4_WriteCommand(1, 67);
				LCD4_WriteCursor(3, 0, (int8_t *)"<< CALIBRATION >>");
				break;
			}
			switch (settingCalibWT)
			{
			case SETTING_WT_STANDARD:
				MODE_Disp_Setting_ASTERISK(2);
				break;
			case SETTING_CLICK_CALIB_WT:
				MODE_Disp_Setting_ASTERISK(3);
				break;
			}
			break;
		case SETTING_CALIB_PH:
			switch (settingCalibPH)
			{
			case SETTING_CALIB_PH_NONE:
				MODE_SLAVE_Disp_Setting_Page3();
				MODE_Disp_Setting_ASTERISK(2);
				break;
			case SETTING_PH_STANDARD:
			case SETTING_CLICK_CALIB_PH:
				MODE_RefreshLcd();
				LCD4_WriteCursor(0, 0, (int8_t *)"Put the probe into");
				LCD4_WriteCursor(1, 0, (int8_t *)"the PH solution");
				sprintf(ac_BuffLcd, "PH solution: %4.2f  ", (double)tDevInfo.nStandard_Ph / 100);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(3, 0, (int8_t *)"<< CALIBRATION >>");
				break;
			case SETTING_CALIB_PH1_A:
			case SETTING_CALIB_PH1_B:
			case SETTING_CALIB_PH2_A:
			case SETTING_CALIB_PH2_B:
				MODE_RefreshLcd();
				sprintf(ac_BuffLcd, "Ph1: %3.2f-> A:%4d", tSenIrriVal.dPh1, tDevInfo.nCalib_Ph1A);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "%4.0f mV   > B:%4d", tSenIrriVal.dPh1mv, tDevInfo.nCalib_Ph1B);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "Ph2: %3.2f-> A:%4d", tSenIrriVal.dPh2, tDevInfo.nCalib_Ph2A);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "%4.0f mV   > B:%4d", tSenIrriVal.dPh2mv, tDevInfo.nCalib_Ph2B);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			default:
				break;
			}
			switch (settingCalibPH)
			{
			case SETTING_PH_STANDARD:
				MODE_Disp_Setting_ASTERISK(2);
				break;
			case SETTING_CLICK_CALIB_PH:
				MODE_Disp_Setting_ASTERISK(3);
				break;
			case SETTING_CALIB_PH1_A:
				MODE_Disp_Setting_ASTERISK(0);
				break;
			case SETTING_CALIB_PH1_B:
				MODE_Disp_Setting_ASTERISK(1);
				break;
			case SETTING_CALIB_PH2_A:
				MODE_Disp_Setting_ASTERISK(2);
				break;
			case SETTING_CALIB_PH2_B:
				MODE_Disp_Setting_ASTERISK(3);
				break;
			}
			break;
		case SETTING_CALIB_DO:
			switch (settingCalibDO)
			{
			case SETTING_CALIB_DO_NONE:
				MODE_SLAVE_Disp_Setting_Page3();
				MODE_Disp_Setting_ASTERISK(3);
				break;
			case SETTING_DO_STANDARD:
			case SETTING_CLICK_CALIB_CALIB_DO:
				MODE_RefreshLcd();
				LCD4_WriteCursor(0, 0, (int8_t *)"Put the probe into");
				LCD4_WriteCursor(1, 0, (int8_t *)"the DO solution");
				sprintf(ac_BuffLcd, "DO solution: %4.2f  ", (double)tDevInfo.nStandard_Do / 100);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(3, 0, (int8_t *)"<< CALIBRATION >>");
				break;
			case SETTING_CALIB_DO1_A:
			case SETTING_CALIB_DO1_B:
			case SETTING_CALIB_DO2_A:
			case SETTING_CALIB_DO2_B:
				MODE_RefreshLcd();
				sprintf(ac_BuffLcd, "Do1: %3.2f-> A:%4d", tSenIrriVal.dDo1, tDevInfo.nCalib_Do1A);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "%4.0f mV   > B:%4d", tSenIrriVal.dDo1mv, tDevInfo.nCalib_Do1B);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "Do2: %3.2f-> A:%4d", tSenIrriVal.dDo2, tDevInfo.nCalib_Do2A);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "%4.0f mV   > B:%4d", tSenIrriVal.dDo2mv, tDevInfo.nCalib_Do2B);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			default:
				break;
			}
			switch (settingCalibDO)
			{
			case SETTING_DO_STANDARD:
				MODE_Disp_Setting_ASTERISK(2);
				break;
			case SETTING_CLICK_CALIB_CALIB_DO:
				MODE_Disp_Setting_ASTERISK(3);
				break;
			case SETTING_CALIB_DO1_A:
				MODE_Disp_Setting_ASTERISK(0);
				break;
			case SETTING_CALIB_DO1_B:
				MODE_Disp_Setting_ASTERISK(1);
				break;
			case SETTING_CALIB_DO2_A:
				MODE_Disp_Setting_ASTERISK(2);
				break;
			case SETTING_CALIB_DO2_B:
				MODE_Disp_Setting_ASTERISK(3);
				break;
			}
			break;
		case SETTING_CALIB_ORP:
			switch (settingCalibORP)
			{
			case SETTING_CALIB_ORP_NODE:
				MODE_SLAVE_Disp_Setting_Page4();
				MODE_Disp_Setting_ASTERISK(1);
				break;
			case SETTING_ORP_STANDARD:
			case SETTING_CLICK_CALIB_CALIB_ORP:
				MODE_RefreshLcd();
				LCD4_WriteCursor(0, 0, (int8_t *)"Put the probe into");
				LCD4_WriteCursor(1, 0, (int8_t *)"the ORP solution");
				sprintf(ac_BuffLcd, "ORP solution: %4.2f  ", (double)tDevInfo.nStandard_Orp / 100);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				LCD4_WriteCursor(3, 0, (int8_t *)"<< CALIBRATION >>");
				break;
			case SETTING_CALIB_ORP1_A:
			case SETTING_CALIB_ORP1_B:
			case SETTING_CALIB_ORP2_A:
			case SETTING_CALIB_ORP2_B:
				MODE_RefreshLcd();
				sprintf(ac_BuffLcd, "Orp1: %3.2f-> A:%4d", tSenIrriVal.dOrp1, tDevInfo.nCalib_Orp1A);
				LCD4_WriteCursor(0, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "%4.0f mV   > B:%4d", tSenIrriVal.dOrp1mv, tDevInfo.nCalib_Orp1B);
				LCD4_WriteCursor(1, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "Orp2: %3.2f-> A:%4d", tSenIrriVal.dOrp2, tDevInfo.nCalib_Orp2A);
				LCD4_WriteCursor(2, 0, (int8_t *)ac_BuffLcd);
				sprintf(ac_BuffLcd, "%4.0f mV   > B:%4d", tSenIrriVal.dOrp2mv, tDevInfo.nCalib_Orp2B);
				LCD4_WriteCursor(3, 0, (int8_t *)ac_BuffLcd);
				break;
			default:
				break;
			}
			switch (settingCalibORP)
			{
			case SETTING_ORP_STANDARD:
				MODE_Disp_Setting_ASTERISK(2);
				break;
			case SETTING_CLICK_CALIB_CALIB_ORP:
				MODE_Disp_Setting_ASTERISK(3);
				break;
			case SETTING_CALIB_ORP1_A:
				MODE_Disp_Setting_ASTERISK(0);
				break;
			case SETTING_CALIB_ORP1_B:
				MODE_Disp_Setting_ASTERISK(1);
				break;
			case SETTING_CALIB_ORP2_A:
				MODE_Disp_Setting_ASTERISK(2);
				break;
			case SETTING_CALIB_ORP2_B:
				MODE_Disp_Setting_ASTERISK(3);
				break;
			}
			break;
		case SETTING_DEFAULT:
			MODE_SLAVE_Disp_Setting_Page4();
			MODE_Disp_Setting_ASTERISK(2);
			break;
		case SETTING_INFO:
			switch (settingInfo)
			{
			case SETTING_INFO_NONE:
				MODE_SLAVE_Disp_Setting_Page4();
				MODE_Disp_Setting_ASTERISK(3);
				break;

			default:
				break;
			}
			break;
		default:
			break;
		}
		break;
	case MODE_CONFIG_SUCCESS:
		LCD4_Clear();
		HAL_Delay(1);
		if (success == true)
			LCD4_WriteCursor(2, 7, (int8_t *)"Saved!");
		else
			LCD4_WriteCursor(2, 7, (int8_t *)"Failed!");
		HAL_Delay(1000);

		LCD4_Clear();
		HAL_Delay(1);

		flagMode = MODE_SETTING;
		settingRf = SETTING_RF_NONE;
		settingThreshold = SETTING_THRESHOLD_NONE;
		settingRtc = SETTING_RTC_NONE;
		settingEXIO = SETTING_EXIO_NONE;
		settingManual = SETTING_MANUAL_NONE;
		settingCalibEC = SETTING_CALIB_EC_NONE;
		settingCalibWT = SETTING_CALIB_WT_NONE;
		settingCalibPH = SETTING_CALIB_PH_NONE;
		settingCalibDO = SETTING_CALIB_DO_NONE;
		settingCalibORP = SETTING_CALIB_ORP_NODE;
		settingInfo = SETTING_INFO_NONE;
		break;
	default:
		break;
	}
}

void MODE_SLAVE_Timer(void)
{
	nTimeRefreshLcd++;
	if (nTimeRefreshLcd > 1200)
		nTimeRefreshLcd = 0;

	switch (ReadButtonMode())
	{
	case BUTTON_MODE_CLICK:
		MODE_SLAVE_HandleButtonMode();
		nTimerCheckDebounce = 0;
		break;
	case BUTTON_MODE_LONGCLICK:
		break;
	default:
		break;
	}

	switch (ReadButtonUp())
	{
	case BUTTON_UP_CLICK:
		MODE_SLAVE_HandleButtonUp();
		nTimerCheckDebounce = 0;
		break;
	case BUTTON_UP_LONGCLICK:
		if (nTimerCheckDebounce > TIMERHANDLELONGCLICK)
		{
			MODE_SLAVE_HandleButtonUp();
			nTimerCheckDebounce = 0;
		}
		else
		{
			nTimerCheckDebounce++;
		}
		break;
	default:
		break;
	}

	switch (ReadButtonDown())
	{
	case BUTTON_DOWN_CLICK:
		MODE_SLAVE_HandleButtonDown();
		nTimerCheckDebounce = 0;
		break;
	case BUTTON_DOWN_LONGCLICK:
		if (nTimerCheckDebounce > TIMERHANDLELONGCLICK)
		{
			MODE_SLAVE_HandleButtonDown();
			nTimerCheckDebounce = 0;
		}
		else
		{
			nTimerCheckDebounce++;
		}
		break;
	default:
		break;
	}

	switch (ReadButtonSet())
	{
	case BUTTON_SET_CLICK:
		MODE_SLAVE_HandleButtonSet();
		nTimerCheckDebounce = 0;
		break;
	case BUTTON_SET_LONGCLICK:
		nTimerCheckDebounce++;
		break;
	default:
		break;
	}
}

void MODE_SLAVE_HandleButtonMode(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		flagMode = MODE_SETTING;
		modeSetting = SETTING_RF;
		settingRf = SETTING_RF_NONE;
		settingThreshold = SETTING_THRESHOLD_NONE;
		settingRtc = SETTING_RTC_NONE;
		settingEXIO = SETTING_EXIO_NONE;
		settingManual = SETTING_MANUAL_NONE;
		settingCalibEC = SETTING_CALIB_EC_NONE;
		settingCalibWT = SETTING_CALIB_WT_NONE;
		settingCalibPH = SETTING_CALIB_PH_NONE;
		settingCalibDO = SETTING_CALIB_DO_NONE;
		settingCalibORP = SETTING_CALIB_ORP_NODE;
		settingInfo = SETTING_INFO_NONE;
		refreshLcd = true;
		break;
	case MODE_SETTING:
		if (settingRf == SETTING_RF_NONE &&
			settingThreshold == SETTING_THRESHOLD_NONE &&
			settingRtc == SETTING_RTC_NONE &&
			settingEXIO == SETTING_EXIO_NONE &&
			settingManual == SETTING_MANUAL_NONE &&
			settingCalibEC == SETTING_CALIB_EC_NONE &&
			settingCalibWT == SETTING_CALIB_WT_NONE &&
			settingCalibPH == SETTING_CALIB_PH_NONE &&
			settingCalibDO == SETTING_CALIB_DO_NONE &&
			settingCalibORP == SETTING_CALIB_ORP_NODE &&
			settingInfo == SETTING_INFO_NONE)
		{
			flagMode = MODE_DISPLAY;
		}
		else if (settingRtc != SETTING_RTC_NONE)
		{
			/* Set RTC */
			//TODO: set rtc

			MODE_Disp_Success(true);
			modeSetting = SETTING_RTC;
		}
		else
		{
			if (settingRf != SETTING_RF_NONE)
			{
				settingRf = SETTING_RF_NONE;
			}
			else if (settingThreshold != SETTING_THRESHOLD_NONE)
			{
				settingThreshold = SETTING_THRESHOLD_NONE;
			}
			else if (settingEXIO != SETTING_EXIO_NONE)
			{
				settingEXIO = SETTING_EXIO_NONE;
			}
			else if (settingManual != SETTING_MANUAL_NONE)
			{
				settingManual = SETTING_MANUAL_NONE;
			}
			else if (settingCalibEC != SETTING_CALIB_EC_NONE)
			{
				settingCalibEC = SETTING_CALIB_EC_NONE;
			}
			else if (settingCalibWT != SETTING_CALIB_WT_NONE)
			{
				settingCalibWT = SETTING_CALIB_WT_NONE;
			}
			else if (settingCalibPH != SETTING_CALIB_PH_NONE)
			{
				settingCalibPH = SETTING_CALIB_PH_NONE;
			}
			else if (settingCalibDO != SETTING_CALIB_DO_NONE)
			{
				settingCalibDO = SETTING_CALIB_DO_NONE;
			}
			else if (settingCalibORP != SETTING_CALIB_ORP_NODE)
			{
				settingCalibORP = SETTING_CALIB_ORP_NODE;
			}
			else if (settingInfo != SETTING_INFO_NONE)
			{
				settingInfo = SETTING_INFO_NONE;
			}

			/* Save data */
			MODE_SLAVE_SaveParams();
			MODE_Disp_Success(true);
		}
		refreshLcd = true;
		break;
	case MODE_CONFIG_SUCCESS:
		break;
	}
}

void MODE_SLAVE_HandleButtonSet(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		break;
	case MODE_SETTING:
		switch (modeSetting)
		{
		case SETTING_RF:
			switch (settingRf)
			{
			case SETTING_RF_NONE:
				settingRf = SETTING_RF_SID;
				refreshLcd = true;
				break;
			case SETTING_RF_SID:
				settingRf = SETTING_RF_DID;
				break;
			case SETTING_RF_DID:
				settingRf = SETTING_RF_PID;
				break;
			case SETTING_RF_PID:
				settingRf = SETTING_RF_CID;
				break;
			case SETTING_RF_CID:
				settingRf = SETTING_RF_TIME_UPLOAD;
				refreshLcd = true;
				break;
			case SETTING_RF_TIME_UPLOAD:
				settingRf = SETTING_RF_SID;
				refreshLcd = true;
				break;
			}
			break;
		case SETTING_THRESHOLD:
			switch (settingThreshold)
			{
			case SETTING_THRESHOLD_NONE:
				settingThreshold = SETTING_THRESHOLD_EC;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_EC:
				settingThreshold = SETTING_THRESHOLD_ECLOW;
				break;
			case SETTING_THRESHOLD_ECLOW:
				settingThreshold = SETTING_THRESHOLD_ECHIGH;
				break;
			case SETTING_THRESHOLD_ECHIGH:
				settingThreshold = SETTING_THRESHOLD_ECDUAL;
				break;
			case SETTING_THRESHOLD_ECDUAL:
				settingThreshold = SETTING_THRESHOLD_WT;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_WT:
				settingThreshold = SETTING_THRESHOLD_WTLOW;
				break;
			case SETTING_THRESHOLD_WTLOW:
				settingThreshold = SETTING_THRESHOLD_WTHIGH;
				break;
			case SETTING_THRESHOLD_WTHIGH:
				settingThreshold = SETTING_THRESHOLD_WTDUAL;
				break;
			case SETTING_THRESHOLD_WTDUAL:
				settingThreshold = SETTING_THRESHOLD_PH;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_PH:
				settingThreshold = SETTING_THRESHOLD_PHLOW;
				break;
			case SETTING_THRESHOLD_PHLOW:
				settingThreshold = SETTING_THRESHOLD_PHHIGH;
				break;
			case SETTING_THRESHOLD_PHHIGH:
				settingThreshold = SETTING_THRESHOLD_PHDUAL;
				break;
			case SETTING_THRESHOLD_PHDUAL:
				settingThreshold = SETTING_THRESHOLD_DO;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_DO:
				settingThreshold = SETTING_THRESHOLD_DOLOW;
				break;
			case SETTING_THRESHOLD_DOLOW:
				settingThreshold = SETTING_THRESHOLD_DOHIGH;
				break;
			case SETTING_THRESHOLD_DOHIGH:
				settingThreshold = SETTING_THRESHOLD_DODUAL;
				break;
			case SETTING_THRESHOLD_DODUAL:
				settingThreshold = SETTING_THRESHOLD_ORP;
				refreshLcd = true;
				break;
			case SETTING_THRESHOLD_ORP:
				settingThreshold = SETTING_THRESHOLD_ORPLOW;
				break;
			case SETTING_THRESHOLD_ORPLOW:
				settingThreshold = SETTING_THRESHOLD_ORPHIGH;
				break;
			case SETTING_THRESHOLD_ORPHIGH:
				settingThreshold = SETTING_THRESHOLD_ORPDUAL;
				break;
			case SETTING_THRESHOLD_ORPDUAL:
				settingThreshold = SETTING_THRESHOLD_EC;
				refreshLcd = true;
				break;
			default:
				break;
			}
			break;
		case SETTING_RTC:
			switch (settingRtc)
			{
			case SETTING_RTC_NONE:
				settingRtc = SETTING_RTC_HOUR;
				refreshLcd = true;
				break;
			case SETTING_RTC_HOUR:
				settingRtc = SETTING_RTC_MINUTE;
				break;
			case SETTING_RTC_MINUTE:
				settingRtc = SETTING_RTC_HOUR;
				break;
			}
			break;
		case SETTING_EXIO:
			switch (settingEXIO)
			{
			case SETTING_EXIO_NONE:

				refreshLcd = true;
				break;
			default:
				break;
			}
			break;
		case SETTING_MANUAL:
			switch (settingManual)
			{
			case SETTING_MANUAL_NONE:

				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_EC:
			switch (settingCalibEC)
			{
			case SETTING_CALIB_EC_NONE:
				fWaterTempStandard = (tSenIrriVal.dWT1 + tSenIrriVal.dWT2) / 2;
				settingCalibEC = SETTING_CALIB_EC_STANDARD;
				refreshLcd = true;
				break;
			case SETTING_CALIB_EC_STANDARD:
				settingCalibEC = SETTING_CALIB_CLICK_CALIB_EC;
				break;
			case SETTING_CALIB_CLICK_CALIB_EC:
				settingCalibEC = SETTING_CALIB_EC1_A;
				refreshLcd = true;
				break;
			case SETTING_CALIB_EC1_A:
				settingCalibEC = SETTING_CALIB_EC2_A;
				break;
			case SETTING_CALIB_EC2_A:
				settingCalibEC = SETTING_CALIB_EC_STANDARD;
				refreshLcd = true;
				break;
			}
			break;
		case SETTING_CALIB_WT:
			switch (settingCalibWT)
			{
			case SETTING_CALIB_WT_NONE:
				refreshLcd = true;
				settingCalibWT = SETTING_WT_STANDARD;
				break;
			case SETTING_WT_STANDARD:
				settingCalibWT = SETTING_CLICK_CALIB_WT;
				break;
			case SETTING_CLICK_CALIB_WT:
				settingCalibWT = SETTING_WT_STANDARD;
				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_PH:
			switch (settingCalibPH)
			{
			case SETTING_CALIB_PH_NONE:
				refreshLcd = true;
				settingCalibPH = SETTING_PH_STANDARD;
				break;
			case SETTING_PH_STANDARD:
				settingCalibPH = SETTING_CLICK_CALIB_PH;
				break;
			case SETTING_CLICK_CALIB_PH:
				refreshLcd = true;
				settingCalibPH = SETTING_CALIB_PH1_A;
				break;
			case SETTING_CALIB_PH1_A:
				settingCalibPH = SETTING_CALIB_PH1_B;
				break;
			case SETTING_CALIB_PH1_B:
				settingCalibPH = SETTING_CALIB_PH2_A;
				break;
			case SETTING_CALIB_PH2_A:
				settingCalibPH = SETTING_CALIB_PH2_B;
				break;
			case SETTING_CALIB_PH2_B:
				refreshLcd = true;
				settingCalibPH = SETTING_PH_STANDARD;
				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_DO:
			switch (settingCalibDO)
			{
			case SETTING_CALIB_DO_NONE:
				refreshLcd = true;
				settingCalibDO = SETTING_DO_STANDARD;
				break;
			case SETTING_DO_STANDARD:
				settingCalibDO = SETTING_CLICK_CALIB_CALIB_DO;
				break;
			case SETTING_CLICK_CALIB_CALIB_DO:
				refreshLcd = true;
				settingCalibDO = SETTING_CALIB_DO1_A;
				break;
			case SETTING_CALIB_DO1_A:
				settingCalibDO = SETTING_CALIB_DO1_B;
				break;
			case SETTING_CALIB_DO1_B:
				settingCalibDO = SETTING_CALIB_DO2_A;
				break;
			case SETTING_CALIB_DO2_A:
				settingCalibDO = SETTING_CALIB_DO2_B;
				break;
			case SETTING_CALIB_DO2_B:
				refreshLcd = true;
				settingCalibDO = SETTING_DO_STANDARD;
				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_ORP:
			switch (settingCalibORP)
			{
			case SETTING_CALIB_ORP_NODE:
				refreshLcd = true;
				settingCalibORP = SETTING_ORP_STANDARD;
				break;
			case SETTING_ORP_STANDARD:
				settingCalibORP = SETTING_CLICK_CALIB_CALIB_ORP;
				break;
			case SETTING_CLICK_CALIB_CALIB_ORP:
				refreshLcd = true;
				settingCalibORP = SETTING_CALIB_ORP1_A;
				break;
			case SETTING_CALIB_ORP1_A:
				settingCalibORP = SETTING_CALIB_ORP1_B;
				break;
			case SETTING_CALIB_ORP1_B:
				settingCalibORP = SETTING_CALIB_ORP2_A;
				break;
			case SETTING_CALIB_ORP2_A:
				settingCalibORP = SETTING_CALIB_ORP2_B;
				break;
			case SETTING_CALIB_ORP2_B:
				refreshLcd = true;
				settingCalibORP = SETTING_ORP_STANDARD;
				break;
			default:
				break;
			}
			break;
		case SETTING_DEFAULT:
			if (nTimerCheckDebounce > 3000)
			{
				sprintf(tDevInfo.cFwVersion, "%s", "TS01A");

				tDevInfo.nSlaveCode = 1;
				tDevInfo.nCM_SID = 100;
				tDevInfo.nCM_DID = 1;
				tDevInfo.nCM_PID = 1;
				tDevInfo.nCM_CID = 33;
				tDevInfo.nCM_RATE = 0;
				tDevInfo.nCM_MHOP = 0;

				tDevInfo.nTimeTransmitSensor = 10;
				tDevInfo.nThrehold_Ec = 160;
				tDevInfo.nThrehold_EcLow = 140;
				tDevInfo.nThrehold_ECHigh = 180;
				tDevInfo.nThrehold_ECDual = 20;

				tDevInfo.nThrehold_WT = 300;
				tDevInfo.nThrehold_WTLow = 250;
				tDevInfo.nThrehold_WTHigh = 350;
				tDevInfo.nThrehold_WTDual = 20;

				tDevInfo.nThrehold_PH = 650;
				tDevInfo.nThrehold_PHLow = 600;
				tDevInfo.nThrehold_PHHigh = 700;
				tDevInfo.nThrehold_PHDual = 20;

				tDevInfo.nThrehold_Do = 500;
				tDevInfo.nThrehold_DoLow = 400;
				tDevInfo.nThrehold_DoHigh = 900;
				tDevInfo.nThrehold_DoDual = 20;

				tDevInfo.nThrehold_Orp = 200;
				tDevInfo.nThrehold_OrpLow = -200;
				tDevInfo.nThrehold_OrpHigh = 1000;
				tDevInfo.nThrehold_OrpDual = 20;

				tDevInfo.nStandard_Ec = 1000;
				tDevInfo.nStandard_Ph = 701;
				tDevInfo.nStandard_Do = 500;
				tDevInfo.nStandard_Orp = 240;

				/* Calib EC */
				tDevInfo.nCalib_Ec1A = 560;
				tDevInfo.nCalib_Ec1B = 0;
				tDevInfo.nCalib_Ec2A = 560;
				tDevInfo.nCalib_Ec2B = 0;

				/* Calib water temp */
				tDevInfo.nCalib_Wt1A = 138;
				tDevInfo.nCalib_Wt1B = 0;
				tDevInfo.nCalib_Wt2A = 138;
				tDevInfo.nCalib_Wt2B = 0;

				/* Calib ph */
				tDevInfo.nCalib_Ph1A = 108;
				tDevInfo.nCalib_Ph1B = 346;
				tDevInfo.nCalib_Ph2A = 108;
				tDevInfo.nCalib_Ph2B = 346;

				/* Calib DO */
				tDevInfo.nCalib_Do1A = 108;
				tDevInfo.nCalib_Do1B = 346;
				tDevInfo.nCalib_Do2A = 108;
				tDevInfo.nCalib_Do2B = 346;

				/* Calib ORP */
				tDevInfo.nCalib_Orp1A = 108;
				tDevInfo.nCalib_Orp1B = 346;
				tDevInfo.nCalib_Orp2A = 108;
				tDevInfo.nCalib_Orp2B = 346;

				/* Save data to flash */
				MODE_SLAVE_SaveParams();
				// MODE_SLAVE_Init();
				refreshLcd = true;
				flagMode = MODE_DISPLAY;
				CMState = CM_CONFIG_CONNECTION;
			}
			refreshLcd = true;
			break;
		case SETTING_INFO:

			break;
		default:
			break;
		}
		break;
	case MODE_CONFIG_SUCCESS:
		break;
	}
}

void MODE_SLAVE_HandleButtonUp(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		switch (modeDisplay)
		{
		case DISPLAY_EC_PH_WT:
			modeDisplay = DISPLAY_DO_ORP;
			refreshLcd = true;
			break;
		case DISPLAY_DO_ORP:
			modeDisplay = DISPLAY_EC_PH_WT;
			refreshLcd = true;
			break;
		default:
			break;
		}
		break;
	case MODE_SETTING:
		switch (modeSetting)
		{
		case SETTING_RF:
			switch (settingRf)
			{
			case SETTING_RF_NONE:
				modeSetting = SETTING_INFO;
				refreshLcd = true;
				break;
			case SETTING_RF_SID:
				if (tDevInfo.nCM_SID >= 0)
				{
					tDevInfo.nCM_SID++;
				}
				break;
			case SETTING_RF_DID:
				if (tDevInfo.nCM_DID >= 0)
				{
					tDevInfo.nCM_DID++;
				}
				break;
			case SETTING_RF_PID:
				if (tDevInfo.nCM_PID >= 0)
				{
					tDevInfo.nCM_PID++;
				}
				break;
			case SETTING_RF_CID:
				if (tDevInfo.nCM_CID >= 0)
				{
					tDevInfo.nCM_CID++;
				}
				break;
			case SETTING_RF_TIME_UPLOAD:
				if (tDevInfo.nTimeUpload >= 0)
				{
					tDevInfo.nTimeUpload++;
				}
				break;
			}
			break;
		case SETTING_THRESHOLD:
			switch (settingThreshold)
			{
			case SETTING_THRESHOLD_NONE:
				modeSetting = SETTING_RF;
				break;
			case SETTING_THRESHOLD_EC:
				if (tDevInfo.nThrehold_Ec >= 0)
				{
					tDevInfo.nThrehold_Ec++;
				}
				break;
			case SETTING_THRESHOLD_ECLOW:
				if (tDevInfo.nThrehold_EcLow >= 0)
				{
					tDevInfo.nThrehold_EcLow++;
				}
				break;
			case SETTING_THRESHOLD_ECHIGH:
				if (tDevInfo.nThrehold_ECHigh >= 0)
				{
					tDevInfo.nThrehold_ECHigh++;
				}
				break;
			case SETTING_THRESHOLD_ECDUAL:
				if (tDevInfo.nThrehold_ECDual >= 0)
				{
					tDevInfo.nThrehold_ECDual++;
				}
				break;
			case SETTING_THRESHOLD_WT:
				if (tDevInfo.nThrehold_WT >= 0)
				{
					tDevInfo.nThrehold_WT++;
				}
				break;
			case SETTING_THRESHOLD_WTLOW:
				if (tDevInfo.nThrehold_WTLow >= 0)
				{
					tDevInfo.nThrehold_WTLow++;
				}
				break;
			case SETTING_THRESHOLD_WTHIGH:
				if (tDevInfo.nThrehold_WTHigh >= 0)
				{
					tDevInfo.nThrehold_WTHigh++;
				}
				break;
			case SETTING_THRESHOLD_WTDUAL:
				if (tDevInfo.nThrehold_WTDual >= 0)
				{
					tDevInfo.nThrehold_WTDual++;
				}
				break;
			case SETTING_THRESHOLD_PH:
				if (tDevInfo.nThrehold_PH >= 0)
				{
					tDevInfo.nThrehold_PH++;
				}
				break;
			case SETTING_THRESHOLD_PHLOW:
				if (tDevInfo.nThrehold_PHLow >= 0)
				{
					tDevInfo.nThrehold_PHLow++;
				}
				break;
			case SETTING_THRESHOLD_PHHIGH:
				if (tDevInfo.nThrehold_PHHigh >= 0)
				{
					tDevInfo.nThrehold_PHHigh++;
				}
				break;
			case SETTING_THRESHOLD_PHDUAL:
				if (tDevInfo.nThrehold_PHDual >= 0)
				{
					tDevInfo.nThrehold_PHDual++;
				}
				break;
			case SETTING_THRESHOLD_DO:
				if (tDevInfo.nThrehold_Do >= 0)
				{
					tDevInfo.nThrehold_Do++;
				}
				break;
			case SETTING_THRESHOLD_DOLOW:
				if (tDevInfo.nThrehold_DoLow >= 0)
				{
					tDevInfo.nThrehold_DoLow++;
				}
				break;
			case SETTING_THRESHOLD_DOHIGH:
				if (tDevInfo.nThrehold_DoHigh >= 0)
				{
					tDevInfo.nThrehold_DoHigh++;
				}
				break;
			case SETTING_THRESHOLD_DODUAL:
				if (tDevInfo.nThrehold_DoDual >= 0)
				{
					tDevInfo.nThrehold_DoDual++;
				}
				break;
			case SETTING_THRESHOLD_ORP:
				if (tDevInfo.nThrehold_Orp >= 0)
				{
					tDevInfo.nThrehold_Orp++;
				}
				break;
			case SETTING_THRESHOLD_ORPLOW:
				if (tDevInfo.nThrehold_OrpLow >= 0)
				{
					tDevInfo.nThrehold_OrpLow++;
				}
				break;
			case SETTING_THRESHOLD_ORPHIGH:
				if (tDevInfo.nThrehold_OrpHigh >= 0)
				{
					tDevInfo.nThrehold_OrpHigh++;
				}
				break;
			case SETTING_THRESHOLD_ORPDUAL:
				if (tDevInfo.nThrehold_OrpDual >= 0)
				{
					tDevInfo.nThrehold_OrpDual++;
				}
				break;
			default:
				break;
			}
			break;
		case SETTING_RTC:
			switch (settingRtc)
			{
			case SETTING_RTC_NONE:
				modeSetting = SETTING_THRESHOLD;
				break;
			default:
				break;
			}
			break;
		case SETTING_EXIO:
			switch (settingEXIO)
			{
			case SETTING_EXIO_NONE:
				modeSetting = SETTING_RTC;
				refreshLcd = true;
				break;
			default:
				break;
			}
			break;
		case SETTING_MANUAL:
			switch (settingManual)
			{
			case SETTING_MANUAL_NONE:
				modeSetting = SETTING_EXIO;
				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_EC:
			switch (settingCalibEC)
			{
			case SETTING_CALIB_EC_NONE:
				modeSetting = SETTING_MANUAL;
				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_WT:
			switch (settingCalibWT)
			{
			case SETTING_CALIB_WT_NONE:
				modeSetting = SETTING_CALIB_EC;
				refreshLcd = true;
				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_PH:
			switch (settingCalibPH)
			{
			case SETTING_CALIB_PH_NONE:
				modeSetting = SETTING_CALIB_WT;
				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_DO:
			switch (settingCalibDO)
			{
			case SETTING_CALIB_DO_NONE:
				modeSetting = SETTING_CALIB_PH;
				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_ORP:
			switch (settingCalibORP)
			{
			case SETTING_CALIB_ORP_NODE:
				modeSetting = SETTING_CALIB_DO;
				refreshLcd = true;
				break;
			default:
				break;
			}
			break;
		case SETTING_DEFAULT:
			modeSetting = SETTING_CALIB_ORP;
			break;
		case SETTING_INFO:
			switch (settingInfo)
			{
			case SETTING_INFO_NONE:
				modeSetting = SETTING_DEFAULT;
				break;
			default:
				break;
			}
			break;
		}
		break;
	case MODE_CONFIG_SUCCESS:
		break;
	}
}

void MODE_SLAVE_HandleButtonDown(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		switch (modeDisplay)
		{
		case DISPLAY_EC_PH_WT:
			modeDisplay = DISPLAY_DO_ORP;
			refreshLcd = true;
			break;
		case DISPLAY_DO_ORP:
			modeDisplay = DISPLAY_EC_PH_WT;
			refreshLcd = true;
			break;
		default:
			break;
		}
		break;
	case MODE_SETTING:
		switch (modeSetting)
		{
		case SETTING_RF:
			switch (settingRf)
			{
			case SETTING_RF_NONE:
				modeSetting = SETTING_THRESHOLD;
				break;
			case SETTING_RF_SID:
				if (tDevInfo.nCM_SID > 0)
				{
					tDevInfo.nCM_SID--;
				}
				break;
			case SETTING_RF_DID:
				if (tDevInfo.nCM_DID > 0)
				{
					tDevInfo.nCM_DID--;
				}
				break;
			case SETTING_RF_PID:
				if (tDevInfo.nCM_PID > 0)
				{
					tDevInfo.nCM_PID--;
				}
				break;
			case SETTING_RF_CID:
				if (tDevInfo.nCM_CID > 0)
				{
					tDevInfo.nCM_CID--;
				}
				break;
			case SETTING_RF_TIME_UPLOAD:
				if (tDevInfo.nTimeUpload > 0)
				{
					tDevInfo.nTimeUpload--;
				}
				break;
			}
			break;
		case SETTING_THRESHOLD:
			switch (settingThreshold)
			{
			case SETTING_THRESHOLD_NONE:
				modeSetting = SETTING_RTC;
				break;
			case SETTING_THRESHOLD_EC:
				if (tDevInfo.nThrehold_Ec > 0)
				{
					tDevInfo.nThrehold_Ec--;
				}
				break;
			case SETTING_THRESHOLD_ECLOW:
				if (tDevInfo.nThrehold_EcLow > 0)
				{
					tDevInfo.nThrehold_EcLow--;
				}
				break;
			case SETTING_THRESHOLD_ECHIGH:
				if (tDevInfo.nThrehold_ECHigh > 0)
				{
					tDevInfo.nThrehold_ECHigh--;
				}
				break;
			case SETTING_THRESHOLD_ECDUAL:
				if (tDevInfo.nThrehold_ECDual > 0)
				{
					tDevInfo.nThrehold_ECDual--;
				}
				break;
			case SETTING_THRESHOLD_WT:
				if (tDevInfo.nThrehold_WT > 0)
				{
					tDevInfo.nThrehold_WT--;
				}
				break;
			case SETTING_THRESHOLD_WTLOW:
				if (tDevInfo.nThrehold_WTLow > 0)
				{
					tDevInfo.nThrehold_WTLow--;
				}
				break;
			case SETTING_THRESHOLD_WTHIGH:
				if (tDevInfo.nThrehold_WTHigh > 0)
				{
					tDevInfo.nThrehold_WTHigh--;
				}
				break;
			case SETTING_THRESHOLD_WTDUAL:
				if (tDevInfo.nThrehold_WTDual > 0)
				{
					tDevInfo.nThrehold_WTDual--;
				}
				break;
			case SETTING_THRESHOLD_PH:
				if (tDevInfo.nThrehold_PH > 0)
				{
					tDevInfo.nThrehold_PH--;
				}
				break;
			case SETTING_THRESHOLD_PHLOW:
				if (tDevInfo.nThrehold_PHLow > 0)
				{
					tDevInfo.nThrehold_PHLow--;
				}
				break;
			case SETTING_THRESHOLD_PHHIGH:
				if (tDevInfo.nThrehold_PHHigh > 0)
				{
					tDevInfo.nThrehold_PHHigh--;
				}
				break;
			case SETTING_THRESHOLD_PHDUAL:
				if (tDevInfo.nThrehold_PHDual > 0)
				{
					tDevInfo.nThrehold_PHDual--;
				}
				break;
			case SETTING_THRESHOLD_DO:
				if (tDevInfo.nThrehold_Do > 0)
				{
					tDevInfo.nThrehold_Do--;
				}
				break;
			case SETTING_THRESHOLD_DOLOW:
				if (tDevInfo.nThrehold_DoLow > 0)
				{
					tDevInfo.nThrehold_DoLow--;
				}
				break;
			case SETTING_THRESHOLD_DOHIGH:
				if (tDevInfo.nThrehold_DoHigh > 0)
				{
					tDevInfo.nThrehold_DoHigh--;
				}
				break;
			case SETTING_THRESHOLD_DODUAL:
				if (tDevInfo.nThrehold_DoDual > 0)
				{
					tDevInfo.nThrehold_DoDual--;
				}
				break;
			case SETTING_THRESHOLD_ORP:
				if (tDevInfo.nThrehold_Orp > 0)
				{
					tDevInfo.nThrehold_Orp--;
				}
				break;
			case SETTING_THRESHOLD_ORPLOW:
				if (tDevInfo.nThrehold_OrpLow > 0)
				{
					tDevInfo.nThrehold_OrpLow--;
				}
				break;
			case SETTING_THRESHOLD_ORPHIGH:
				if (tDevInfo.nThrehold_OrpHigh > 0)
				{
					tDevInfo.nThrehold_OrpHigh--;
				}
				break;
			case SETTING_THRESHOLD_ORPDUAL:
				if (tDevInfo.nThrehold_OrpDual > 0)
				{
					tDevInfo.nThrehold_OrpDual--;
				}
				break;
			default:
				break;
			}
			break;
		case SETTING_RTC:
			switch (settingRtc)
			{
			case SETTING_RTC_NONE:
				modeSetting = SETTING_EXIO;
				refreshLcd = true;
				break;
			default:
				break;
			}
			break;
		case SETTING_EXIO:
			switch (settingEXIO)
			{
			case SETTING_EXIO_NONE:
				modeSetting = SETTING_MANUAL;
				break;
			default:
				break;
			}
			break;
		case SETTING_MANUAL:
			switch (settingManual)
			{
			case SETTING_MANUAL_NONE:
				modeSetting = SETTING_CALIB_EC;
				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_EC:
			switch (settingCalibEC)
			{
			case SETTING_CALIB_EC_NONE:
				modeSetting = SETTING_CALIB_WT;
				refreshLcd = true;
				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_WT:
			switch (settingCalibWT)
			{
			case SETTING_CALIB_WT_NONE:
				modeSetting = SETTING_CALIB_PH;
				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_PH:
			switch (settingCalibPH)
			{
			case SETTING_CALIB_PH_NONE:
				modeSetting = SETTING_CALIB_DO;
				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_DO:
			switch (settingCalibDO)
			{
			case SETTING_CALIB_DO_NONE:
				modeSetting = SETTING_CALIB_ORP;
				refreshLcd = true;
				break;
			default:
				break;
			}
			break;
		case SETTING_CALIB_ORP:
			switch (settingCalibORP)
			{
			case SETTING_CALIB_ORP_NODE:
				modeSetting = SETTING_DEFAULT;
				break;
			default:
				break;
			}
			break;
		case SETTING_DEFAULT:
			modeSetting = SETTING_INFO;
			break;
		case SETTING_INFO:
			switch (settingInfo)
			{
			case SETTING_INFO_NONE:
				modeSetting = SETTING_RF;
				refreshLcd = true;
				break;
			default:
				break;
			}
			break;
		}
		break;
	case MODE_CONFIG_SUCCESS:
		break;
	}
}

void MODE_SLAVE_LoadParams(void)
{
	int index = 0;

	MY_FLASH_ReadN(index, &tDevInfo.nSlaveCode, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCM_SID, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCM_DID, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCM_PID, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCM_CID, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCM_RATE, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCM_MHOP, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nTimeTransmitSensor, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_Ec, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_EcLow, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_ECHigh, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_ECDual, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_WT, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_WTLow, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_WTHigh, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_WTDual, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_PH, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_PHLow, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_PHHigh, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_PHDual, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_Do, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_DoLow, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_DoHigh, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_DoDual, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_Orp, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_OrpLow, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_OrpHigh, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nThrehold_OrpDual, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Ec1A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Ec1B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Ec2A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Ec2B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Wt1A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Wt1B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Wt2A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Wt2B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Ph1A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Ph1B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Ph2A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Ph2B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Do1A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Do1B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Do2A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Do2B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Orp1A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Orp1B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Orp2A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCalib_Orp2B, 1, DATA_TYPE_32);
	index += 4;
}

void MODE_SLAVE_SaveParams(void)
{
	int index = 0;

	MY_FLASH_EraseSector();
	HAL_FLASH_Unlock();

	MY_FLASH_WriteN(index, &tDevInfo.nSlaveCode, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCM_SID, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCM_DID, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCM_PID, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCM_CID, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCM_RATE, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCM_MHOP, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nTimeTransmitSensor, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_Ec, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_EcLow, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_ECHigh, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_ECDual, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_WT, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_WTLow, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_WTHigh, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_WTDual, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_PH, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_PHLow, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_PHHigh, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_PHDual, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_Do, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_DoLow, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_DoHigh, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_DoDual, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_Orp, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_OrpLow, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_OrpHigh, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nThrehold_OrpDual, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Ec1A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Ec1B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Ec2A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Ec2B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Wt1A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Wt1B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Wt2A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Wt2B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Ph1A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Ph1B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Ph2A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Ph2B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Do1A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Do1B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Do2A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Do2B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Orp1A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Orp1B, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Orp2A, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCalib_Orp2B, 1, DATA_TYPE_32);
	index += 4;

	HAL_FLASH_Lock();
}

void MODE_MASTER_SaveParams(void)
{
	int index = 300;

	MY_FLASH_EraseSector();
	HAL_FLASH_Unlock();

	MY_FLASH_WriteN(index, &tDevInfo.nMasterCode, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCM_SID, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCM_DID, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCM_PID, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCM_CID, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCM_RATE, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nCM_MHOP, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_WriteN(index, &tDevInfo.nTimeUpload, 1, DATA_TYPE_32);
	index += 4;

	HAL_FLASH_Lock();
}

void MODE_MASTER_LoadParams(void)
{
	int index = 300;

	MY_FLASH_ReadN(index, &tDevInfo.nMasterCode, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCM_SID, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCM_DID, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCM_PID, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCM_CID, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCM_RATE, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nCM_MHOP, 1, DATA_TYPE_32);
	index += 4;
	MY_FLASH_ReadN(index, &tDevInfo.nTimeUpload, 1, DATA_TYPE_32);
	index += 4;
}

void MODE_MASTER_Timer(void)
{
	nTimeRefreshLcd++;
	if (nTimeRefreshLcd > 1200)
		nTimeRefreshLcd = 0;

	switch (ReadButtonMode())
	{
	case BUTTON_MODE_CLICK:
		MODE_MASTER_HandleButtonMode();
		nTimerCheckDebounce = 0;
		break;
	case BUTTON_MODE_LONGCLICK:
		break;
	default:
		break;
	}

	switch (ReadButtonUp())
	{
	case BUTTON_UP_CLICK:
		MODE_MASTER_HandleButtonUp();
		nTimerCheckDebounce = 0;
		break;
	case BUTTON_UP_LONGCLICK:
		if (nTimerCheckDebounce > TIMERHANDLELONGCLICK)
		{
			MODE_MASTER_HandleButtonUp();
			nTimerCheckDebounce = 0;
		}
		else
		{
			nTimerCheckDebounce++;
		}
		break;
	default:
		break;
	}

	switch (ReadButtonDown())
	{
	case BUTTON_DOWN_CLICK:
		MODE_MASTER_HandleButtonDown();
		nTimerCheckDebounce = 0;
		break;
	case BUTTON_DOWN_LONGCLICK:
		if (nTimerCheckDebounce > TIMERHANDLELONGCLICK)
		{
			MODE_MASTER_HandleButtonDown();
			nTimerCheckDebounce = 0;
		}
		else
		{
			nTimerCheckDebounce++;
		}
		break;
	default:
		break;
	}

	switch (ReadButtonSet())
	{
	case BUTTON_SET_CLICK:
		MODE_MASTER_HandleButtonSet();
		nTimerCheckDebounce = 0;
		break;
	case BUTTON_SET_LONGCLICK:
		nTimerCheckDebounce++;
		break;
	default:
		break;
	}
}

void MODE_MASTER_HandleButtonMode(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		flagMode = MODE_SETTING;
		modeSetting = SETTING_RF;
		settingRf = SETTING_RF_NONE;
		settingEthernet = SETTING_ETHERNET_NONE;
		settingRtc = SETTING_RTC_NONE;
		settingInfo = SETTING_INFO_NONE;
		refreshLcd = true;
		break;
	case MODE_SETTING:
		if (settingRf != SETTING_RF_NONE)
		{
			modeSetting = SETTING_RF;
			settingRf = SETTING_RF_NONE;
			MODE_Disp_Success(true);
		}
		else if (settingEthernet != SETTING_ETHERNET_NONE)
		{
			modeSetting = SETTING_INTERNET;
			settingEthernet = SETTING_ETHERNET_NONE;
			MODE_Disp_Success(true);
		}
		else if (settingRtc != SETTING_RTC_NONE)
		{
			modeSetting = SETTING_RTC;
			settingRtc = SETTING_RTC_NONE;
			//TODO: set rtc
		}
		else if (settingInfo != SETTING_INFO_NONE)
		{
			modeSetting = SETTING_INFO;
			settingInfo = SETTING_INFO_NONE;
		}
		else
		{
			flagMode = MODE_DISPLAY;
		}

		MODE_MASTER_SaveParams();
		refreshLcd = true;
		break;
	case MODE_CONFIG_SUCCESS:
		break;
	}
}

void MODE_MASTER_HandleButtonSet(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		break;
	case MODE_SETTING:
		switch (modeSetting)
		{
		case SETTING_RF:
			switch (settingRf)
			{
			case SETTING_RF_NONE:
				settingRf = SETTING_RF_SID;
				refreshLcd = true;
				break;
			case SETTING_RF_SID:
				settingRf = SETTING_RF_DID;
				break;
			case SETTING_RF_DID:
				settingRf = SETTING_RF_PID;
				break;
			case SETTING_RF_PID:
				settingRf = SETTING_RF_CID;
				break;
			case SETTING_RF_CID:
				settingRf = SETTING_RF_SID;
				break;
			}
			break;
		case SETTING_INTERNET:
			switch (settingEthernet)
			{
			case SETTING_ETHERNET_NONE:
				settingEthernet = SETTING_ETHERNET_TIME_UPLOAD;
				refreshLcd = true;
				break;
			case SETTING_ETHERNET_TIME_UPLOAD:
				settingEthernet = SETTING_ETHERNET_RESET;
				break;
			case SETTING_ETHERNET_RESET:
				settingEthernet = SETTING_ETHERNET_TIME_UPLOAD;
				break;
			}
			break;
		case SETTING_RTC:
			switch (settingRtc)
			{
			case SETTING_RTC_NONE:
				settingRtc = SETTING_RTC_HOUR;
				refreshLcd = true;
				break;
			case SETTING_RTC_HOUR:
				settingRtc = SETTING_RTC_MINUTE;
				break;
			case SETTING_RTC_MINUTE:
				settingRtc = SETTING_RTC_HOUR;
				break;
			}
			break;
		case SETTING_DEFAULT:
			if (nTimerCheckDebounce > 3000)
			{
				sprintf(tDevInfo.cFwVersion, "%s", "TM01A");

				tDevInfo.nCM_SID = 1;
				tDevInfo.nCM_DID = 100;
				tDevInfo.nCM_PID = 1;
				tDevInfo.nCM_CID = 33;
				tDevInfo.nCM_RATE = 0;
				tDevInfo.nCM_MHOP = 0;
				tDevInfo.nMasterCode = 0;
				tDevInfo.nTimeUpload = 300;

				MODE_MASTER_SaveParams();
				// MODE_MASTER_Init();
				refreshLcd = true;
				flagMode = MODE_DISPLAY;
				CMState = CM_CONFIG_CONNECTION;
			}
			break;
		case SETTING_INFO:
			break;
		}
	}
}

void MODE_MASTER_HandleButtonUp(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		/* code */
		break;
	case MODE_SETTING:
		switch (modeSetting)
		{
		case SETTING_RF:
			switch (settingRf)
			{
			case SETTING_RF_NONE:
				modeSetting = SETTING_INFO;
				refreshLcd = true;
				break;
			case SETTING_RF_SID:
				if (tDevInfo.nCM_SID >= 0)
				{
					tDevInfo.nCM_SID++;
				}
				break;
			case SETTING_RF_DID:
				if (tDevInfo.nCM_DID >= 0)
				{
					tDevInfo.nCM_DID++;
				}
				break;
			case SETTING_RF_PID:
				if (tDevInfo.nCM_PID >= 0)
				{
					tDevInfo.nCM_PID++;
				}
				break;
			case SETTING_RF_CID:
				if (tDevInfo.nCM_CID >= 0)
				{
					tDevInfo.nCM_CID++;
				}
				break;
			default:
				break;
			}
			break;
		case SETTING_INTERNET:
			switch (settingEthernet)
			{
			case SETTING_ETHERNET_NONE:
				modeSetting = SETTING_RF;
				break;
			case SETTING_ETHERNET_TIME_UPLOAD:
				if (tDevInfo.nTimeUpload >= 0)
				{
					tDevInfo.nTimeUpload++;
				}
				break;
			case SETTING_ETHERNET_RESET:
				break;
			default:
				break;
			}
			break;
		case SETTING_RTC:
			switch (settingRtc)
			{
			case SETTING_RTC_NONE:
				modeSetting = SETTING_INTERNET;
				break;
			case SETTING_RTC_HOUR:
				break;
			case SETTING_RTC_MINUTE:
				break;
			default:
				break;
			}
			break;
		case SETTING_DEFAULT:
			modeSetting = SETTING_RTC;
			refreshLcd = true;
			break;
		case SETTING_INFO:
			switch (settingInfo)
			{
			case SETTING_INFO_NONE:
				modeSetting = SETTING_DEFAULT;
				break;
			default:
				break;
			}
			break;
		}
	case MODE_CONFIG_SUCCESS:
		break;
	}
}

void MODE_MASTER_HandleButtonDown(void)
{
	switch (flagMode)
	{
	case MODE_DISPLAY:
		/* code */
		break;
	case MODE_SETTING:
		switch (modeSetting)
		{
		case SETTING_RF:
			switch (settingRf)
			{
			case SETTING_RF_NONE:
				modeSetting = SETTING_INTERNET;
				break;
			case SETTING_RF_SID:
				if (tDevInfo.nCM_SID > 0)
				{
					tDevInfo.nCM_SID--;
				}
				break;
			case SETTING_RF_DID:
				if (tDevInfo.nCM_DID > 0)
				{
					tDevInfo.nCM_DID--;
				}
				break;
			case SETTING_RF_PID:
				if (tDevInfo.nCM_PID > 0)
				{
					tDevInfo.nCM_PID--;
				}
				break;
			case SETTING_RF_CID:
				if (tDevInfo.nCM_CID > 0)
				{
					tDevInfo.nCM_CID--;
				}
				break;
			default:
				break;
			}
			break;
		case SETTING_INTERNET:
			switch (settingEthernet)
			{
			case SETTING_ETHERNET_NONE:
				modeSetting = SETTING_RTC;
				break;
			case SETTING_ETHERNET_TIME_UPLOAD:
				if (tDevInfo.nTimeUpload > 0)
				{
					tDevInfo.nTimeUpload--;
				}
				break;
			case SETTING_ETHERNET_RESET:
				break;
			default:
				break;
			}
			break;
		case SETTING_RTC:
			switch (settingRtc)
			{
			case SETTING_RTC_NONE:
				modeSetting = SETTING_DEFAULT;
				refreshLcd = true;
				break;
			case SETTING_RTC_HOUR:
				break;
			case SETTING_RTC_MINUTE:
				break;
			default:
				break;
			}
			break;
		case SETTING_DEFAULT:
			modeSetting = SETTING_INFO;
			break;
		case SETTING_INFO:
			switch (settingInfo)
			{
			case SETTING_INFO_NONE:
				modeSetting = SETTING_RF;
				refreshLcd = true;
				break;
			default:
				break;
			}
			break;
		}
	case MODE_CONFIG_SUCCESS:
		break;
	}
}
