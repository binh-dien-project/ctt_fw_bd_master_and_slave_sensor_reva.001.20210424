/*
 * mode.h
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#ifndef __MODE_H
#define __MODE_H

#ifdef STM32F407xx
#include "stm32f4xx_hal.h"
#endif
#ifdef STM32F103xx
#include "stm32f1xx_hal.h"
#endif
#include <stdlib.h>
#include <stdbool.h>

typedef enum
{
	ERR_NONE = 1,
	ERR_CM_CONFIG_FAIL,
	ERR_WIFI_DISCONNECTED,
} errCode_t;

typedef enum
{
	MODE_DISPLAY = 10,
	MODE_SETTING,
	MODE_CONFIG_SUCCESS
} flagMode_t;
typedef enum
{
	DISPLAY_EC_PH_WT = 13,
	DISPLAY_DO_ORP,
	DISPLAY_EC_PH_HU_SOIL,
	DISPLAY_TE_HU,
	DISPLAY_MASTER
} modeDisplay_t;
typedef enum
{
	SETTING_INTERNET = 1,
	SETTING_RF,
	SETTING_THRESHOLD,
	SETTING_RTC,
	SETTING_EXIO,
	SETTING_MANUAL,
	SETTING_CALIB_EC,
	SETTING_CALIB_WT,
	SETTING_CALIB_PH,
	SETTING_CALIB_DO,
	SETTING_CALIB_ORP,
	SETTING_CALIB_TE,
	SETTING_CALIB_HU,
	SETTING_CALIB_EC_SOIL,
	SETTING_CALIB_PH_SOIL,
	SETTING_CALIB_HU_SOIL,
	SETTING_DEFAULT,
	SETTING_INFO
} modeSetting_t;

typedef enum
{
	SETTING_RF_NONE = 1,
	SETTING_RF_SID,
	SETTING_RF_DID,
	SETTING_RF_PID,
	SETTING_RF_CID,
	SETTING_RF_TIME_UPLOAD
}
settingRf_t;

typedef enum
{
	SETTING_ETHERNET_NONE = 1,
	SETTING_ETHERNET_RESET,
	SETTING_ETHERNET_TIME_UPLOAD
} settingEthernet_t;

typedef enum
{
	SETTING_THRESHOLD_NONE = 10,
	SETTING_THRESHOLD_EC,
	SETTING_THRESHOLD_ECLOW,
	SETTING_THRESHOLD_ECHIGH,
	SETTING_THRESHOLD_ECDUAL,
	SETTING_THRESHOLD_WT,
	SETTING_THRESHOLD_WTLOW,
	SETTING_THRESHOLD_WTHIGH,
	SETTING_THRESHOLD_WTDUAL,
	SETTING_THRESHOLD_PH,
	SETTING_THRESHOLD_PHLOW,
	SETTING_THRESHOLD_PHHIGH,
	SETTING_THRESHOLD_PHDUAL,
	SETTING_THRESHOLD_DO,
	SETTING_THRESHOLD_DOLOW,
	SETTING_THRESHOLD_DOHIGH,
	SETTING_THRESHOLD_DODUAL,
	SETTING_THRESHOLD_ORP,
	SETTING_THRESHOLD_ORPLOW,
	SETTING_THRESHOLD_ORPHIGH,
	SETTING_THRESHOLD_ORPDUAL,
	SETTING_THRESHOLD_TE,
	SETTING_THRESHOLD_TELOW,
	SETTING_THRESHOLD_TEHIGH,
	SETTING_THRESHOLD_TEDUAL,
	SETTING_THRESHOLD_HU,
	SETTING_THRESHOLD_EC_SOIL,
	SETTING_THRESHOLD_PH_SOIL,
	SETTING_THRESHOLD_HU_SOIL
} settingThreshold_t;
typedef enum
{
	SETTING_RTC_NONE = 25,
	SETTING_RTC_HOUR,
	SETTING_RTC_MINUTE
} settingRtc_t;
typedef enum
{
	SETTING_EXIO_NONE = 30,
} settingEXIO_t;
typedef enum
{
	SETTING_MANUAL_NONE = 40,
} settingManual_t;
typedef enum
{
	SETTING_CALIB_EC_NONE = 50,
	SETTING_CALIB_EC1_A,
	SETTING_CALIB_EC_STANDARD,
	SETTING_CALIB_CLICK_CALIB_EC,
	SETTING_CALIB_EC2_A,
} settingCalibEC_t;
typedef enum
{
	SETTING_CALIB_WT_NONE = 60,
	SETTING_WT_STANDARD,
	SETTING_CLICK_CALIB_WT
} settingCalibWT_t;
typedef enum
{
	SETTING_CALIB_PH_NONE = 70,
	SETTING_PH_STANDARD,
	SETTING_CLICK_CALIB_PH,
	SETTING_CALIB_PH1_A,
	SETTING_CALIB_PH1_B,
	SETTING_CALIB_PH2_A,
	SETTING_CALIB_PH2_B
} settingCalibPH_t;
typedef enum
{
	SETTING_CALIB_DO_NONE = 80,
	SETTING_DO_STANDARD,
	SETTING_CLICK_CALIB_CALIB_DO,
	SETTING_CALIB_DO1_A,
	SETTING_CALIB_DO1_B,
	SETTING_CALIB_DO2_A,
	SETTING_CALIB_DO2_B,
} settingCalibDO_t;
typedef enum
{
	SETTING_CALIB_ORP_NODE = 100,
	SETTING_ORP_STANDARD,
	SETTING_CLICK_CALIB_CALIB_ORP,
	SETTING_CALIB_ORP1_A,
	SETTING_CALIB_ORP1_B,
	SETTING_CALIB_ORP2_A,
	SETTING_CALIB_ORP2_B,
} settingCalibORP_t;
typedef enum
{
	SETTING_CALIB_TE_NONE = 110,
	SETTING_CALIB_TE_STAND
} settingCalibTE_t;
typedef enum
{
	SETTING_CALIB_HU_NONE = 120,
	SETTING_CALIB_HU_STAND
} settingCalibHU_t;
typedef enum
{
	SETTING_CALIB_EC_SOIL_NONE = 130,
} settingCalibEcSoil_t;
typedef enum
{
	SETTING_CALIB_PH_SOIL_NONE = 140,
} settingCalibPhSoil_t;
typedef enum
{
	SETTING_CALIB_HU_SOIL_NONE = 150,
} settingCalibHuSoil_t;
typedef enum
{
	SETTING_INFO_NONE = 243,
	SETTING_INFO_FWVERSION = 241,
	SETTING_INFO_FARMCODE = 242
} settingInfo_t;

typedef struct
{
	char cFwVersion[5];

	int32_t nMasterCode;
	int32_t nSlaveCode;
	int32_t nCM_SID;
	int32_t nCM_DID;
	int32_t nCM_PID;
	int32_t nCM_CID;
	int32_t nCM_RATE;
	int32_t nCM_MHOP;

	int32_t nDateTime_Hour;
	int32_t nDateTime_Minute;
	int32_t nTimeUpload;
	int32_t nTimeTransmitSensor;
	int32_t nThrehold_Ec;
	int32_t nThrehold_EcLow;
	int32_t nThrehold_ECHigh;
	int32_t nThrehold_ECDual;
	int32_t nThrehold_WT;
	int32_t nThrehold_WTLow;
	int32_t nThrehold_WTHigh;
	int32_t nThrehold_WTDual;
	int32_t nThrehold_PH;
	int32_t nThrehold_PHLow;
	int32_t nThrehold_PHHigh;
	int32_t nThrehold_PHDual;
	int32_t nThrehold_Do;
	int32_t nThrehold_DoLow;
	int32_t nThrehold_DoHigh;
	int32_t nThrehold_DoDual;
	int32_t nThrehold_Orp;
	int32_t nThrehold_OrpLow;
	int32_t nThrehold_OrpHigh;
	int32_t nThrehold_OrpDual;

	int32_t nStandard_Ec;
	int32_t nStandard_Ph;
	int32_t nStandard_Do;
	int32_t nStandard_Orp;
	
	int32_t nCalib_Ec1A;
	int32_t nCalib_Ec1B;
	int32_t nCalib_Ec2A;
	int32_t nCalib_Ec2B;
	int32_t nCalib_Wt1A;
	int32_t nCalib_Wt1B;
	int32_t nCalib_Wt2A;
	int32_t nCalib_Wt2B;
	int32_t nCalib_Ph1A;
	int32_t nCalib_Ph1B;
	int32_t nCalib_Ph2A;
	int32_t nCalib_Ph2B;
	int32_t nCalib_Do1A;
	int32_t nCalib_Do1B;
	int32_t nCalib_Do2A;
	int32_t nCalib_Do2B;
	int32_t nCalib_Orp1A;
	int32_t nCalib_Orp1B;
	int32_t nCalib_Orp2A;
	int32_t nCalib_Orp2B;

	int32_t nThrehold_EcSoil;
	int32_t nThrehold_EcSoilLow;
	int32_t nThrehold_ECSoilHigh;
	int32_t nThrehold_ECSoilDual;
	int32_t nThrehold_PHSoil;
	int32_t nThrehold_PHSoilLow;
	int32_t nThrehold_PHSoilHigh;
	int32_t nThrehold_PHSoilDual;
	int32_t nThrehold_HuSoil;
	int32_t nThrehold_HuSoilLow;
	int32_t nThrehold_HuSoilHigh;
	int32_t nThrehold_HuSoilDual;
	int32_t nThrehold_Te;
	int32_t nThrehold_TeLow;
	int32_t nThrehold_TeHigh;
	int32_t nThrehold_TeDual;
	int32_t nThrehold_Hu;
	int32_t nThrehold_HuLow;
	int32_t nThrehold_HuHigh;
	int32_t nThrehold_HuDual;
} _DEV_INFO;

/* Function Prototype */
void MODE_SLAVE_Init(void);
void MODE_SLAVE_Lcd(void);
void MODE_SLAVE_Timer(void);
void MODE_SLAVE_LoadParams(void);
void MODE_SLAVE_SaveParams(void);

void MODE_MASTER_Init(void);
void MODE_MASTER_Lcd(void);
void MODE_MASTER_Timer(void);
void MODE_MASTER_LoadParams(void);
void MODE_MASTER_SaveParams(void);


#endif
