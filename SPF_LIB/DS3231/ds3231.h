/*
 * ds3231.h
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#ifndef __DS3231_H
#define __DS3231_H

#ifdef STM32F407xx
#include "stm32f4xx_hal.h"
#endif
#ifdef STM32F103xx
#include "stm32f1xx_hal.h"
#endif
#include <stdlib.h>
#include <stdbool.h>

#define RTC_RESET_PORT	GPIOD
#define RTC_RESET_PIN	GPIO_PIN_2
#define DS3131_RESET(x)	HAL_GPIO_WritePin(RTC_RESET_PORT, RTC_RESET_PIN, x);

#define DS3231_REG_TIME         0x00
#define DS3231_REG_ALARM1       0x07
#define DS3231_REG_ALARM2       0x0B
#define DS3231_REG_CONTROL      0x0E
#define DS3231_REG_STATUS       0x0F
#define DS3231_REG_TEMP         0x11

#define DS3231_CON_EOSC         0x80
#define DS3231_CON_BBSQW        0x40
#define DS3231_CON_CONV         0x20
#define DS3231_CON_RS2          0x10
#define DS3231_CON_RS1          0x08
#define DS3231_CON_INTCN        0x04
#define DS3231_CON_A2IE         0x02
#define DS3231_CON_A1IE         0x01

#define DS3231_STA_OSF          0x80
#define DS3231_STA_32KHZ        0x08
#define DS3231_STA_BSY          0x04
#define DS3231_STA_A2F          0x02
#define DS3231_STA_A1F          0x01

typedef enum
{
	DS3231_IDLE = 1,
	DS3231_GET_TIME,
	DS3231_SET_TIME,
	DS3231_RESET_MASTER,
	DS3231_RESET_SLAVE
} Ds3231_t;

typedef enum
{
  ALARM_MODE_ALL_MATCHED = 0,
  ALARM_MODE_HOUR_MIN_SEC_MATCHED,
  ALARM_MODE_MIN_SEC_MATCHED,
  ALARM_MODE_SEC_MATCHED,
  ALARM_MODE_ONCE_PER_SECOND
} AlarmMode;

typedef enum
{
  SUNDAY = 1,
  MONDAY,
  TUESDAY,
  WEDNESDAY,
  THURSDAY,
  FRIDAY,
  SATURDAY
} DaysOfWeek;

typedef struct
{
  int8_t Year;
  int8_t Month;
  int8_t Date;
  int8_t DaysOfWeek;
  int8_t Hour;
  int8_t Min;
  int8_t Sec;
	Ds3231_t State;
} _DS3231;

bool DS3231_Process(void);
bool DS3231_Timer(void);
void DS3231_Init(I2C_HandleTypeDef *handle);
bool DS3231_GetTime(_DS3231 *rtc);
bool DS3231_SetTime(_DS3231 *rtc);
bool DS3231_ReadTemperature(float *temp);
bool DS3231_SetAlarm1(AlarmMode mode, uint8_t date, uint8_t hour, uint8_t min, uint8_t sec);
bool DS3231_ClearAlarm1(void);
bool DS3231_ReadRegister(uint8_t regAddr, uint8_t *value);
bool DS3231_WriteRegister(uint8_t regAddr, uint8_t value);

#endif
