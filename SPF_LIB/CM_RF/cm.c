/*
 * cm.c
 *
 *  Created on: Jan 23, 2021
 *      Author: Zeder
 */

#include "include.h"

#define MAX_STRING 1000
#define DATA_LENGHT	130
//#define TRANSMITTER

extern _SENSOR_IRRI tSenIrriVal;
extern _DEV_INFO tDevInfo;
extern errCode_t errCode;

CMState_t CMState;
static UART_HandleTypeDef *uart;

uint16_t test;

uint8_t CM_REC_DATA;
uint16_t nCmTrasmitData;
char pDataRecv[MAX_STRING];
char pStr[DATA_LENGHT];
uint16_t nCMSysInfoTimerCount;
uint16_t nCMTransmitTimerCount;
uint16_t nCMCheckTimerCount;
uint16_t nCMPeriodTimerCount;

static bool CM_Slave_Send_SensorCollect(void);
static bool CM_Master_Request_SensorCollect(void);
static void CM_Read(uint8_t *pData, uint16_t Size);
static void CM_Write(uint8_t *pData, uint16_t Size);
static void CM_AcceptReceive(void);
static void CM_AbortReceive(void);
static char *subString(char *s, int pos, int index);
static void CM_Slave_ConfigID(void);
static void CM_Master_ConfigID(void);
static void CM_ConfigID(int reg, int cmd);
static void CM_SendCommand(char *cmd);

char *subString(char *s, int pos, int index)
{
	char *t = &s[pos];
	s[pos - 1] = '\0';
	for (int i = index; i < (strlen(t) + 1); i++)
	{
		t[i] = '\0';
	}
	return t;
}

bool getStrAt(const char *str, char sep, int index, char *ret)
{
	unsigned int start = 0, stop;
	int i = 0;

	for (stop = 0; str[stop]; stop++)
	{
		if (str[stop] == sep)
		{
			if (i == index)
			{
				strncpy(ret, str + start, stop - start);
			}
			else
			{
				i++;
				start = stop + 1;
			}
		}
	}

	return true;
}

bool CM_Master_Request_SensorCollect(void)
{
	bool ret;
	char *data;
	data = (char *)malloc(5 * sizeof(char));

	if (data == NULL)
	{
		return false;
	}

	getStrAt(pStr, ',', 0, data);
	if ((data[0] == 'S') && (data[1] == 'T'))
	{
		getStrAt(pStr, ',', 16, data);
//		if ((data[0] == 'E') && (data[1] == 'D'))
//		{
			getStrAt(pStr, ',', 2, data);
			tSenIrriVal.dEc1 = atof(&data[0]);
			getStrAt(pStr, ',', 3, data);
			tSenIrriVal.dEc2 = atof(&data[0]);
			getStrAt(pStr, ',', 4, data);
			tSenIrriVal.dPh1 = atof(&data[0]);
			getStrAt(pStr, ',', 5, data);
			tSenIrriVal.dPh2 = atof(&data[0]);
			getStrAt(pStr, ',', 6, data);
			tSenIrriVal.dWT1 = atof(&data[0]);
			getStrAt(pStr, ',', 7, data);
			tSenIrriVal.dWT2 = atof(&data[0]);
			getStrAt(pStr, ',', 8, data);
			tSenIrriVal.dDo1 = atof(&data[0]);
			getStrAt(pStr, ',', 9, data);
			tSenIrriVal.dDo2 = atof(&data[0]);
			getStrAt(pStr, ',', 10, data);
			tSenIrriVal.dOrp1 = atof(&data[0]);
			getStrAt(pStr, ',', 11, data);
			tSenIrriVal.dOrp2 = atof(&data[0]);
			getStrAt(pStr, ',', 12, data);
			tSenIrriVal.dBat1mAmpe = atof(&data[0]);
			getStrAt(pStr, ',', 13, data);
			tSenIrriVal.dSol1mAmpe = atof(&data[0]);
			getStrAt(pStr, ',', 14, data);
			tSenIrriVal.dBat1mv = atof(&data[0]);
			getStrAt(pStr, ',', 15, data);
			tSenIrriVal.dSol1mv = atof(&data[0]);
			
			ret = true;
//		}
//		else
//		{
//			ret = false;
//		}
	}
	else
	{
		ret = false;
	}

	free(data);

	return ret;
}
bool CM_Slave_Send_SensorCollect(void)
{
	char *msg;
	msg = (char *)malloc(470 * sizeof(char));

	if (msg == NULL)
	{
		return false;
	}

	char data[100];
	//
	//Format Data
	//send=ST,nSlaveCode,pEC1,pEC2,pPH1,pPH2,pWT1,pWT2,pORP1,pORP2,pDO1,pDO2,pBattV1,pBattV2,pBattC1,pBattC2,pSolV1,pSolV2,pSolC1,pSolC2,ED
	
	//Sensor transmit
	//send=ST,nSlaveCode,SEUP,pEC1,pEC2,pPH1,pPH2,pSoilTemp1,pSoilTemp2,pSoilHumi1,pSoilHumi2,pCliTemp1,pCliTemp2,pCliHumi1,pCliHumi2,pBattV1,pBattV2,pBattC1,pBattC2,pSolV1,pSolV2,pSolC1,pSolC2,ED
	
	//Actuator transmit
	//send=ST,nSlaveCode,SACT,IO1,IO2,IO3,IO4,IO5,IO6,IO7,IO8,IO9,IO10,ED
	
	//Actuator Receive
	//send=ST,nSlaveCode,MACT,IO1,IO2,IO3,IO4,IO5,IO6,IO7,IO8,IO9,IO10,ED
	//
	sprintf((char *)msg, "%s", CM_SEND_COMMAND);

	sprintf(data, "%s", "ST,");
	strcat(msg, data);

	//Code
	sprintf(data, "%03d,", (int)tDevInfo.nSlaveCode);
	strcat(msg, data);

	//EC
	sprintf(data, "%01.02f,%01.02f,", tSenIrriVal.dEc1, tSenIrriVal.dEc2);
	strcat(msg, data);
	//PH
	sprintf(data, "%01.02f,%01.02f,", tSenIrriVal.dPh1, tSenIrriVal.dPh2);
	strcat(msg, data);
	//WT
	sprintf(data, "%02.02f,%02.02f,", tSenIrriVal.dWT1, tSenIrriVal.dWT2);
	strcat(msg, data);
	//DO
	sprintf(data, "%02.01f,%02.01f,", tSenIrriVal.dDo1, tSenIrriVal.dDo2);
	strcat(msg, data);
	//ORP
	sprintf(data, "%04f,%04f,", tSenIrriVal.dOrp1, tSenIrriVal.dOrp2);
	strcat(msg, data);
	// Current
	sprintf(data, "%01.02f,%01.02f,", tSenIrriVal.dBat1mAmpe, tSenIrriVal.dSol1mAmpe);
	strcat(msg, data);
	// Voltage
	sprintf(data, "%02.01f,%02.01f,", tSenIrriVal.dBat1mv, tSenIrriVal.dSol1mv);
	strcat(msg, data);

	sprintf(data, "%s", "ED\r");
	strcat(msg, data);

	//
	//Send through uart
	//
	CM_Write((uint8_t *)msg, strlen((const char *)msg));

	free(msg);
	return true;
}
void CM_Write(uint8_t *pData, uint16_t Size)
{
	HAL_UART_Transmit(uart, pData, Size, 1000);
}

void CM_Read(uint8_t *pData, uint16_t Size)
{
	HAL_UART_Receive(uart, pData, Size, 2000);
}
void CM_AcceptReceive(void)
{
	HAL_UART_Receive_DMA(uart, (uint8_t *)pDataRecv, MAX_STRING);
}
void CM_AbortReceive(void)
{
	HAL_UART_AbortReceive(uart);
}

void CM_Slave_Init(UART_HandleTypeDef *handle)
{
	//	uint8_t msg[50];

	uart = handle;
	//	CM_Slave_ConfigID();

	//	CM_SendCommand((char *)CM_SYSREG);
	CM_SendCommand((char *)CM_SENDING_MODE);
	CM_AcceptReceive();
	CMState = CM_WAIT_FOR_TRANS;
	nCMCheckTimerCount = 5;
}

void CM_Master_Init(UART_HandleTypeDef *handle)
{
	uart = handle;

	CM_SendCommand((char *)CM_SYSREG);
	CM_AcceptReceive();
	CMState = CM_CHECK_CONFIG;
	nCMCheckTimerCount = 5;
}

void CM_Slave_Process(void)
{
	switch (CMState)
	{
	case CM_ERROR_CONFIG:
		break;
	case CM_CONFIG_CONNECTION:
		//		memset(pDataRecv, '\0', (size_t)strlen((char *)pDataRecv));
		memset(pDataRecv, '\0', MAX_STRING);
		//		CM_Slave_ConfigID();
		CMState = CM_CHECK_CONFIG;
		nCMCheckTimerCount = 5; //TODO: CM - Timer check connection
		break;
	case CM_CHECK_CONFIG:
		if (strlen((char *)pDataRecv) > 500) //TODO: CM - Check condition for check slave config
		{
			char *sid_str = subString(pDataRecv, 14, 4);
			char *did_str = subString(pDataRecv, 35, 4);
			char *pid_str = subString(pDataRecv, 56, 4);
			char *cid_str = subString(pDataRecv, 79, 2);

			tDevInfo.nCM_SID = atoi(&sid_str[0]);
			tDevInfo.nCM_DID = atoi(&did_str[0]);
			tDevInfo.nCM_PID = atoi(&pid_str[0]);
			tDevInfo.nCM_CID = atoi(&cid_str[0]);

			CMState = CM_WAIT_FOR_TRANS;
		}
		else if (nCMCheckTimerCount == 0)
		{
			CMState = CM_ERROR_CONFIG;
		}
		break;
	case CM_WAIT_FOR_TRANS:
		if (nCMPeriodTimerCount == 0)
		{
			if (nCMSysInfoTimerCount == 0)
			{
				CMState = CM_TRANS_SYSINFO;
			}
			else
			{
				CMState = CM_TRANS_SENSOR;
			}

			nCMPeriodTimerCount = tDevInfo.nTimeTransmitSensor;
		}

		break;
	case CM_TRANS_SENSOR:
		if (nCMTransmitTimerCount == 0)
		{
			CM_Slave_Send_SensorCollect();
			CMState = CM_WAIT_FOR_TRANS;
		}
		break;
	case CM_TRANS_SYSINFO:
		//TODO: transmit sysinfo

		nCMSysInfoTimerCount = 300; //TODO: change timer transmit sysinfo
		nCMTransmitTimerCount = 2;
		CMState = CM_TRANS_SENSOR;

		break;
	default:
		break;
	}
}

void CM_Slave_Timer1000ms(void)
{
	if (nCMCheckTimerCount != 0)
		nCMCheckTimerCount--;
	if (nCMTransmitTimerCount != 0)
		nCMTransmitTimerCount--;
	if (nCMSysInfoTimerCount != 0)
		nCMSysInfoTimerCount--;
	if (nCMPeriodTimerCount != 0)
		nCMPeriodTimerCount--;
}

void CM_Master_Process(void)
{
	//	uint8_t msg[20];

	switch (CMState)
	{
	case CM_ERROR_CONFIG:
		break;
	case CM_CONFIG_CONNECTION:
		CMState = CM_CHECK_CONFIG;
		//		memset(pDataRecv, '\0', (size_t)MAX_STRING);
		//		CM_ConfigID((int)1,(int)tDevInfo.nCM_SID);
		//		CM_ConfigID((int)2,(int)tDevInfo.nCM_DID);
		//		CM_ConfigID((int)3,(int)tDevInfo.nCM_PID);
		//		CM_ConfigID((int)4,(int)tDevInfo.nCM_CID);
		//		CM_ConfigID((int)5,(int)tDevInfo.nCM_RATE);
		//		CM_ConfigID((int)30,(int)tDevInfo.nCM_MHOP);
		//		HAL_Delay(100);
		//
		//		memset(pDataRecv, '\0', (size_t)MAX_STRING);
		//		CM_AcceptReceive();
		//		CM_SendCommand(CM_SYSREG);
		//		HAL_Delay(100);
		//		sprintf((char *)msg, "%s\r", CM_SYSREG);
		//		HAL_UART_Transmit(uart, msg, strlen((const char *)msg), 1000);

		nCMCheckTimerCount = 5; //TODO: CM - Timer check connection
		break;
	case CM_CHECK_CONFIG:
		test = strlen((char *)pDataRecv);
		if (strlen((char *)pDataRecv) > 0) //TODO: CM - Check condition for check slave config
		{
			char *sid_str = subString(pDataRecv, 14, 4);
			char *did_str = subString(pDataRecv, 35, 4);
			char *pid_str = subString(pDataRecv, 56, 4);
			char *cid_str = subString(pDataRecv, 79, 2);

			tDevInfo.nCM_SID = atoi(&sid_str[0]);
			tDevInfo.nCM_DID = atoi(&did_str[0]);
			tDevInfo.nCM_PID = atoi(&pid_str[0]);
			tDevInfo.nCM_CID = atoi(&cid_str[0]);

			MODE_MASTER_SaveParams();
			memset(pDataRecv, '\0', MAX_STRING);
			CM_SendCommand((char *)CM_LISTEN);
			CMState = CM_RECV;
		}
		else if (nCMCheckTimerCount == 0)
		{
			tDevInfo.nCM_SID = 0;
			tDevInfo.nCM_DID = 0;
			tDevInfo.nCM_PID = 0;
			tDevInfo.nCM_CID = 0;

			CMState = CM_ERROR_CONFIG;
		}
		break;
	case CM_RECV:
		test = strlen((char *)pDataRecv);
		if (strlen((char *)pDataRecv) > 100)
		{
			memset(pStr, '\0', DATA_LENGHT);
			sprintf(pStr, "%s", strstr((const char *)pDataRecv, "ST"));
			memset(pDataRecv, '\0', MAX_STRING);
			CM_Master_Request_SensorCollect();
			CM_AbortReceive();
			CM_AcceptReceive();
		}
		else
		{
			memset(pDataRecv, '\0', MAX_STRING);
			CM_AbortReceive();
			CM_AcceptReceive();
		}

		break;
	default:
		break;
	}
}

void CM_Master_Timer1000ms(void)
{
	if (nCMCheckTimerCount != 0)
		nCMCheckTimerCount--;
}

void CM_Slave_ConfigID(void)
{
	uint8_t msg[50];

	sprintf((char *)msg, "%s%d,%04d\r", CM_SYSREG, 1, (int)tDevInfo.nCM_SID);
	CM_Write(msg, strlen((const char *)msg));
	HAL_Delay(100);
	sprintf((char *)msg, "%s%d,%04d\r", CM_SYSREG, 2, (int)tDevInfo.nCM_DID);
	CM_Write(msg, strlen((const char *)msg));
	HAL_Delay(100);
	sprintf((char *)msg, "%s%d,%04d\r", CM_SYSREG, 3, (int)tDevInfo.nCM_PID);
	CM_Write(msg, strlen((const char *)msg));
	HAL_Delay(100);
	sprintf((char *)msg, "%s%d,%04d\r", CM_SYSREG, 4, (int)tDevInfo.nCM_CID);
	CM_Write(msg, strlen((const char *)msg));
	HAL_Delay(100);
	sprintf((char *)msg, "%s%d,%04d\r", CM_SYSREG, 5, (int)tDevInfo.nCM_RATE);
	CM_Write(msg, strlen((const char *)msg));
	HAL_Delay(100);
	sprintf((char *)msg, "%s%d,%04d\r", CM_SYSREG, 30, (int)tDevInfo.nCM_MHOP);
	CM_Write(msg, strlen((const char *)msg));

	HAL_Delay(100);
	sprintf((char *)msg, "%s\r", CM_SYSREG);
	CM_Write(msg, strlen((const char *)msg));

	HAL_Delay(100);

	sprintf((char *)msg, "%s\r", CM_SENDING_MODE);
	CM_Write(msg, strlen((const char *)msg));
}

void CM_ConfigID(int reg, int cmd)
{
	uint8_t msg[20];

	sprintf((char *)msg, "%s%d,%d\r", CM_SYSREG, reg, cmd);
	CM_Write(msg, strlen((const char *)msg));
	// HAL_Delay(100);
}

void CM_SendCommand(char *cmd)
{
	uint8_t msg[20];

	sprintf((char *)msg, "%s\r", cmd);
	CM_Write(msg, strlen((const char *)msg));
	// HAL_Delay(100);
}

void CM_Master_ConfigID(void)
{
	uint8_t msg[50];

	sprintf((char *)msg, "%s%d,%04d\r", CM_SYSREG, 1, (int)tDevInfo.nCM_SID);
	CM_Write(msg, strlen((const char *)msg));
	HAL_Delay(100);
	sprintf((char *)msg, "%s%d,%04d\r", CM_SYSREG, 2, (int)tDevInfo.nCM_DID);
	CM_Write(msg, strlen((const char *)msg));
	HAL_Delay(100);
	sprintf((char *)msg, "%s%d,%04d\r", CM_SYSREG, 3, (int)tDevInfo.nCM_PID);
	CM_Write(msg, strlen((const char *)msg));
	HAL_Delay(100);
	sprintf((char *)msg, "%s%d,%04d\r", CM_SYSREG, 4, (int)tDevInfo.nCM_CID);
	CM_Write(msg, strlen((const char *)msg));
	HAL_Delay(100);
	sprintf((char *)msg, "%s%d,%04d\r", CM_SYSREG, 5, (int)tDevInfo.nCM_RATE);
	CM_Write(msg, strlen((const char *)msg));
	HAL_Delay(100);
	sprintf((char *)msg, "%s%d,%04d\r", CM_SYSREG, 30, (int)tDevInfo.nCM_MHOP);
	CM_Write(msg, strlen((const char *)msg));

	HAL_Delay(100);
	sprintf((char *)msg, "%s\r", CM_SYSREG);
	CM_Write(msg, strlen((const char *)msg));
	HAL_Delay(100);

	sprintf((char *)msg, "%s\r", CM_LISTEN);
	CM_Write(msg, strlen((const char *)msg));
}
