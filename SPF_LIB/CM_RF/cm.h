/*
 * cm.h
 *
 *  Created on: Jan 23, 2021
 *      Author: Zeder
 */

#ifndef _CM_H_
#define _CM_H_

#ifdef STM32F407xx
#include "stm32f4xx_hal.h"
#endif
#ifdef STM32F103xx
#include "stm32f1xx_hal.h"
#endif
#include <stdlib.h>
#include <stdbool.h>

#define CM_SYSREG "sysreg="
#define CM_LISTEN "listen"
#define CM_BUAD_CONFIG "baud="
#define CM_BUAD_INFORMATION "baud"
#define CM_SENDING_MODE "ms"
#define CM_SEND_COMMAND "send="
#define CM_SELF_ID "SelfID="
#define CM_DEST_ID "DestID="
#define CM_PAN_ID "PANID="
#define CM_CHANNEL_ID "Ch="

typedef enum
{
	CM_TRANS_SENSOR = 1,
	CM_WAIT_FOR_TRANS,
	CM_TRANS_SYSINFO,
	CM_CHECK_CONFIG,
	CM_CONFIG_CONNECTION,
	CM_ERROR_CONFIG,
	CM_RECV
} CMState_t;

#ifdef SPF_SLAVE
void CM_Slave_Init(UART_HandleTypeDef *handle);
void CM_Slave_Process(void);
void CM_Slave_Config(void);
void CM_Slave_Timer1000ms(void);
#else
void CM_Master_Init(UART_HandleTypeDef *handle);
void CM_Master_Config(void);
void CM_Master_Process(void);
void CM_Master_Timer1000ms(void);
#endif

#endif /*_CM_H_*/
