/*
 * flash.h
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#ifndef __FLASH_H
#define __FLASH_H

#ifdef STM32F407xx
#include "stm32f4xx_hal.h"
#endif
#ifdef STM32F103xx
#include "stm32f1xx_hal.h"
#endif
#include <stdlib.h>
#include <stdbool.h>

#define EEPROM_START_ADDRESS ((uint32_t)0x080E0000)

//Typedefs
//1. data size
typedef enum
{
	DATA_TYPE_8=0,
	DATA_TYPE_16,
	DATA_TYPE_32,
}DataTypeDef;

//functions prototypes
//1. Erase Sector
void MY_FLASH_EraseSector(void);

//2. Set Sector Adress
void MY_FLASH_SetSectorAddrs(uint8_t sector, uint32_t addrs);
//3. Write Flash
void MY_FLASH_WriteN(uint32_t idx, void *wrBuf, uint32_t Nsize, DataTypeDef dataType);
//4. Read Flash
void MY_FLASH_ReadN(uint32_t idx, void *rdBuf, uint32_t Nsize, DataTypeDef dataType);

#endif
