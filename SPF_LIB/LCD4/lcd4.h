/*
 * lcd4.h
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#ifndef __LCD4_H
#define __LCD4_H

#ifdef STM32F407xx
#include "stm32f4xx_hal.h"
#endif
#ifdef STM32F103xx
#include "stm32f1xx_hal.h"
#endif

#include <stdlib.h>
#include <stdbool.h>

#define LCD_D4_PORT GPIOE
#define LCD_D4_PIN	GPIO_PIN_0
#define LCD_D5_PORT	GPIOE
#define LCD_D5_PIN	GPIO_PIN_1
#define LCD_D6_PORT	GPIOE
#define LCD_D6_PIN	GPIO_PIN_2
#define LCD_D7_PORT	GPIOE
#define LCD_D7_PIN	GPIO_PIN_3
#define LCD_RS_PORT	GPIOE
#define LCD_RS_PIN	GPIO_PIN_4
#define LCD_EN_PORT	GPIOE
#define LCD_EN_PIN	GPIO_PIN_5

#define HD44780_SETCGRAMADDR 	((uint8_t)0x40U)

#define PIN_HIGH(PORT,PIN)		HAL_GPIO_WritePin(PORT,PIN,GPIO_PIN_SET);
#define PIN_LOW(PORT,PIN)		HAL_GPIO_WritePin(PORT,PIN,GPIO_PIN_RESET);
//---------DECLARE------------------
//#define LCD16xN //For LCD 16xN
#define LCD20xN //For LCD 20xN

//---------FUNCTION PROTOTYPE-------
void LCD4_Init(void);
void LCD4_WriteCursor(uint8_t x, uint8_t y, int8_t *string);
bool LCD4_LoadCustomChar(uint8_t cell, uint8_t *charMap);
void LCD4_WriteCommand(uint8_t type,uint8_t data);
void LCD4_Clear(void);

#endif
