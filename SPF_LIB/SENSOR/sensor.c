/*
*@Name: sensor.c
*@Description: Read irrigation sensor: Ph, Ec, Water Temp, DO, ORP
*@NTC2: PA0 - ADC_IN0
*@NTC1: PA1 - ADC_IN1
*@ORP1: PA2 - ADC_IN2
*@EC2: PA3 - ADC_IN3
*@ORP2: PA4 - ADC_IN4
*@PH2: PA5 - ADC_IN5
*@DO2: PA6 - ADC_IN6
*@DO1: PA7 - ADC_IN7
*@PH1: PB0 - ADC_IN8
*@EC1: PB1 - ADC_IN9
*@SOL_CUR: PC1 - ADC_IN11
*@BAT_CUR: PC2 - ADC_IN12
*@BAT_VOL: PC3 - ADC_IN13
*@SOL_VOL: PC4 - ADC_IN14
*/

#include "../include.h"


extern _DEV_INFO tDevInfo;
_SENSOR_IRRI tSenIrriVal;


__IO uint32_t nBufAdc0[14];
__IO uint32_t nBufAdc1[14];

__IO long double dEc1Sum;
__IO long double dWT1Sum;
__IO long double dEc2Sum;
__IO long double dWT2Sum;
__IO long double dPh1Sum;
__IO long double dPh2Sum;
__IO long double dDo1Sum;
__IO long double dDo2Sum;
__IO long double dOrp1Sum;
__IO long double dOrp2Sum;
__IO long double dBat1mAmpeSum;
__IO long double dBat1mvSum;
__IO long double dSol1mAmpeSum;
__IO long double dSol1mvSum;
__IO uint16_t nCountIrriSample;
__IO uint16_t nCountEc1Sample;
__IO uint16_t nCountWt1Sample;
__IO uint16_t nCountEc2Sample;
__IO uint16_t nCountWt2Sample;
__IO uint16_t nCountPh1Sample;
__IO uint16_t nCountPh2Sample;
__IO uint16_t nCountDo1Sample;
__IO uint16_t nCountDo2Sample;
__IO uint16_t nCountOrp1Sample;
__IO uint16_t nCountOrp2Sample;
__IO uint16_t nCountBat1mAmpeSample;
__IO uint16_t nCountBat1mvSample;
__IO uint16_t nCountSol1mAmpeSample;
__IO uint16_t nCountSol1mvSample;

// Const for water temperature NTC 10K
const uint16_t RT0 = 10000; //(Ohm)
const uint16_t B = 3977;	//(K)
const float T0 = 25 + 273.15;
double VRT, VR, RT, ln;

// Const for ec compensation by water temp
const float a = 0.019;

void SENSOR_IrriInit(ADC_HandleTypeDef *handle1, ADC_HandleTypeDef *handle2)
{
	HAL_ADC_Start_DMA(handle1, (uint32_t *)&nBufAdc0, 14);
	HAL_ADC_Start_DMA(handle2, (uint32_t *)&nBufAdc1, 14);
}

void SENSOR_IrriInitSingle(ADC_HandleTypeDef *handle1)
{
	HAL_ADC_Start_DMA(handle1, (uint32_t *)&nBufAdc0, 1);
}

bool SENSOR_IrriRead(void)
{
	if (nCountIrriSample < 1000)
	{
		/* Read NTC1 ADC Value */
		if ((nBufAdc0[1] != 0)&&(nBufAdc1[1] != 0))
		{
			if (fabs(nBufAdc0[1] - nBufAdc1[1]) < 100)
			{
				dWT1Sum += (nBufAdc0[1] + nBufAdc1[1])/2;
				nCountWt1Sample++;
			}
		}

		/* Read NTC2 ADC Value */
		if ((nBufAdc0[0] != 0)&&(nBufAdc1[0] != 0))
		{
			if (fabs(nBufAdc0[0] - nBufAdc1[0]) < 100)
			{
				dWT2Sum += (nBufAdc0[0] + nBufAdc1[0])/2;
				nCountWt2Sample++;
			}
		}

		/* Read PH1 ADC Value */
		if ((nBufAdc0[8] != 0)&&(nBufAdc1[8] != 0))
		{
			if (fabs(nBufAdc0[8] - nBufAdc1[8]) < 100)
			{
				dPh1Sum += (nBufAdc0[8] + nBufAdc1[8])/2;
				nCountPh1Sample++;
			}
		}

		/* Read PH2 ADC Value */
		if ((nBufAdc0[5] != 0)&&(nBufAdc1[5] != 0))
		{
			if (fabs(nBufAdc0[5] - nBufAdc1[5]) < 100)
			{
				dPh2Sum += (nBufAdc0[5] + nBufAdc1[5])/2;
				nCountPh2Sample++;
			}
		}

		/* Read EC1 ADC Value */
		if ((nBufAdc0[9] != 0)&&(nBufAdc1[9] != 0))
		{
			if (fabs(nBufAdc0[9] - nBufAdc1[9]) < 100)
			{
				dEc1Sum += (nBufAdc0[9] + nBufAdc1[9])/2;
				nCountEc1Sample++;
			}
		}

		/* Read EC2 ADC Value */
		if ((nBufAdc0[3] != 0)&&(nBufAdc1[3] != 0))
		{
			if (fabs(nBufAdc0[3] - nBufAdc1[3]) < 100)
			{
				dEc2Sum += (nBufAdc0[3] + nBufAdc1[3])/2;
				nCountEc2Sample++;
			}
		}

		/* Read DO1 ADC Value */
		if ((nBufAdc0[7] != 0)&&(nBufAdc1[7] != 0))
		{
			if (fabs(nBufAdc0[7] - nBufAdc1[7]) < 100)
			{
				dDo1Sum += (nBufAdc0[7] + nBufAdc1[7])/2;
				nCountDo1Sample++;
			}
		}

		/* Read DO2 ADC Value */
		if ((nBufAdc0[6] != 0)&&(nBufAdc1[6] != 0))
		{
			if (fabs(nBufAdc0[6] - nBufAdc1[6]) < 100)
			{
				dDo2Sum += (nBufAdc0[6] + nBufAdc1[6])/2;
				nCountDo2Sample++;
			}
		}

		/* Read ORP1 ADC Value */
		if ((nBufAdc0[2] != 0)&&(nBufAdc1[2] != 0))
		{
			if (fabs(nBufAdc0[2] - nBufAdc1[2]) < 100)
			{
				dOrp1Sum += (nBufAdc0[2] + nBufAdc1[2])/2;
				nCountOrp1Sample++;
			}
		}

		/* Read ORP2 ADC Value */
		if ((nBufAdc0[4] != 0)&&(nBufAdc1[4] != 0))
		{
			if (fabs(nBufAdc0[4] - nBufAdc1[4]) < 100)
			{
				dOrp2Sum += (nBufAdc0[4] + nBufAdc1[4])/2;
				nCountOrp2Sample++;
			}
		}

		/* Read battery current */
		if ((nBufAdc0[11] != 0)&&(nBufAdc1[11] != 0))
		{
			if (fabs(nBufAdc0[11] - nBufAdc1[11]) < 100)
			{
				dBat1mAmpeSum += (nBufAdc0[11] + nBufAdc1[11])/2;
				nCountBat1mAmpeSample++;
			}
		}

		/* Read battery voltage */
		if ((nBufAdc0[12] != 0)&&(nBufAdc1[12] != 0))
		{
			if (fabs(nBufAdc0[12] - nBufAdc1[12]) < 200)
			{
				dBat1mvSum += (nBufAdc0[12] + nBufAdc1[12])/2;
				nCountBat1mvSample++;
			}
		}

		/* Read solar current */
		if ((nBufAdc0[10] != 0)&&(nBufAdc1[10] != 0))
		{
			if (fabs(nBufAdc0[10] - nBufAdc1[10]) < 100)
			{
				dSol1mAmpeSum += (nBufAdc0[10] + nBufAdc1[10])/2;
				nCountSol1mAmpeSample++;
			}
		}

		/* Read solar voltage */
		if ((nBufAdc0[13] != 0)&&(nBufAdc1[13] != 0))
		{
			if (fabs(nBufAdc0[13] - nBufAdc1[13]) < 100)
			{
				dSol1mvSum += (nBufAdc0[13] + nBufAdc1[13])/2;
				nCountSol1mvSample++;
			}
		}

		nCountIrriSample++;
	}
	else
	{
		// water temp 1
		if (nCountWt1Sample != 0)
		{
			VRT = dWT1Sum / nCountWt1Sample;	  // acquisition analog value of VRT
			tSenIrriVal.dWT1mv = (3300 * VRT)/4096; // conversion to voltage
			VR = 5000 - tSenIrriVal.dWT1mv;
			RT = (tSenIrriVal.dWT1mv * RT0) / VR; // RT0 = R = 10KOhm
			ln = log(RT / RT0);
			tSenIrriVal.dWT1 = (1 / ((ln / B) + (1 / T0))); // Temperature from thermistor
			tSenIrriVal.dWT1 -= 273.15;
			tSenIrriVal.dWT1 += (double)tDevInfo.nCalib_Wt1B/100;
		}
		else
		{
			tSenIrriVal.dWT1 = 0;
		}

		// water temp 2
		if (nCountWt2Sample != 0)
		{
			VRT = dWT2Sum / nCountWt2Sample;	  // acquisition analog value of VRT
			tSenIrriVal.dWT2mv = (3300 * VRT)/4096; // conversion to voltage
			VR = 5000 - tSenIrriVal.dWT2mv;
			RT = (tSenIrriVal.dWT2mv * RT0) / VR; // RT0 = R = 10KOhm
			ln = log(RT / RT0);
			tSenIrriVal.dWT2 = (1 / ((ln / B) + (1 / T0))); // Temperature from thermistor
			tSenIrriVal.dWT2 -= 273.15;
			tSenIrriVal.dWT2 += (double)tDevInfo.nCalib_Wt2B/100;
		}
		else
		{
			tSenIrriVal.dWT2 = 0;
		}

		// ph1
		if (nCountPh1Sample != 0)
		{
			// TEST
			tDevInfo.nCalib_Ph1A = 108;
			tDevInfo.nCalib_Ph1B = 346;

			tSenIrriVal.dPh1mv = dPh1Sum / nCountPh1Sample;
			tSenIrriVal.dPh1mv = tSenIrriVal.dPh1mv * 3300 / 4096;
			tSenIrriVal.dPh1 = (double)tDevInfo.nCalib_Ph1A * tSenIrriVal.dPh1mv / 100000 + (double)tDevInfo.nCalib_Ph1B / 100;
			if (tSenIrriVal.dPh1 > 14)
				tSenIrriVal.dPh1 = 14;
			else if (tSenIrriVal.dPh1 < 0)
				tSenIrriVal.dPh1 = 0;
		}
		else
		{
			tSenIrriVal.dPh1 = 0;
		}

		// ph2
		if (nCountPh2Sample != 0)
		{
			// TEST
			tDevInfo.nCalib_Ph2A = 108;
			tDevInfo.nCalib_Ph2B = 346;

			tSenIrriVal.dPh2mv = dPh2Sum / nCountPh2Sample;
			tSenIrriVal.dPh2mv = tSenIrriVal.dPh2mv * 3300 / 4096;
			tSenIrriVal.dPh2 = (double)tDevInfo.nCalib_Ph2A * tSenIrriVal.dPh2mv / 100000 + (double)tDevInfo.nCalib_Ph2B / 100;
			if (tSenIrriVal.dPh2 > 14)
				tSenIrriVal.dPh2 = 14;
			else if (tSenIrriVal.dPh2 < 0)
				tSenIrriVal.dPh2 = 0;
		}
		else
		{
			tSenIrriVal.dPh2 = 0;
		}

		// ec1
		if (nCountEc1Sample != 0)
		{
			// TEST
			tDevInfo.nCalib_Ec1A = 800;
			tDevInfo.nCalib_Ec1B = 0;

			tSenIrriVal.dEc1mv = dEc1Sum/nCountEc1Sample;
			tSenIrriVal.dEc1 = (double)tDevInfo.nCalib_Ec1A*tSenIrriVal.dEc1mv/100/1000 + (double)tDevInfo.nCalib_Ec1B/100;
			tSenIrriVal.dEc1 /= (double)(1 + a * tSenIrriVal.dWT1 - a * (T0 - 273.15)); // calib at T0 degree
			if (tSenIrriVal.dEc1 < 0)
				tSenIrriVal.dEc1 = 0;
		}
		else
		{
			tSenIrriVal.dEc1 = 0;
		}

		// ec2
		if (nCountEc2Sample != 0)
		{
			// TEST
			tDevInfo.nCalib_Ec2A = 800;
			tDevInfo.nCalib_Ec2B = 0;

			tSenIrriVal.dEc2mv = dEc2Sum/nCountEc2Sample;
			tSenIrriVal.dEc2 = (double)tDevInfo.nCalib_Ec2A*tSenIrriVal.dEc2mv/100/1000 + (double)tDevInfo.nCalib_Ec2B/100;
			tSenIrriVal.dEc2 /= (double)(1 + a * tSenIrriVal.dWT2 - a * (T0 - 273.15)); // calib at T0 degree
			if (tSenIrriVal.dEc2 < 0)
				tSenIrriVal.dEc2 = 0;
		}
		else
		{
			tSenIrriVal.dEc2 = 0;
		}

		// do1
		if (nCountDo1Sample != 0)
			tSenIrriVal.dDo1 = dDo1Sum/nCountDo1Sample;
		else
			tSenIrriVal.dDo1 = 0;

		// do2
		if (nCountDo2Sample != 0)
			tSenIrriVal.dDo2 = dEc2Sum/nCountDo2Sample;
		else
			tSenIrriVal.dDo2 = 0;

		// orp1
		if (nCountOrp1Sample != 0)
		{
			// test
			tDevInfo.nCalib_Orp1A = 1;
			tDevInfo.nCalib_Orp1B = 1;

			tSenIrriVal.dOrp1mv = dOrp1Sum/nCountOrp1Sample;
			tSenIrriVal.dOrp1mv = tSenIrriVal.dOrp1mv * 3300 / 4096;
			tSenIrriVal.dOrp1 = (double)tDevInfo.nCalib_Orp1A*tSenIrriVal.dOrp1mv + (double)tDevInfo.nCalib_Orp1B/100;

		}
		else
		{
			tSenIrriVal.dOrp1 = 0;
		}

		// orp2
		if (nCountOrp2Sample != 0)
		{
			// test
			tDevInfo.nCalib_Orp2A = 1;
			tDevInfo.nCalib_Orp2B = 1;

			tSenIrriVal.dOrp2mv = dOrp2Sum/nCountOrp2Sample;
			tSenIrriVal.dOrp2mv = tSenIrriVal.dOrp2mv * 3300 / 4096;
			tSenIrriVal.dOrp2 = (double)tDevInfo.nCalib_Orp2A*tSenIrriVal.dOrp2mv + (double)tDevInfo.nCalib_Orp2B/100;

		}
		else
		{
			tSenIrriVal.dOrp2 = 0;
		}

		// BATTERY CURRENT
		if (nCountBat1mAmpeSample != 0)
		{
			tSenIrriVal.dBat1mAmpe = dBat1mAmpeSum/nCountBat1mAmpeSample;
			tSenIrriVal.dBat1mAmpe *= 3300;
			tSenIrriVal.dBat1mAmpe /= 4096;
			tSenIrriVal.dBat1mAmpe = (11.2) * tSenIrriVal.dBat1mAmpe - 30000;
		}
		else
		{
			tSenIrriVal.dBat1mAmpe = 0;
		}

		// BATTERY VOLTAGE
		if (nCountBat1mvSample != 0)
		{
			tSenIrriVal.dBat1mv = dBat1mvSum/nCountBat1mvSample;
			tSenIrriVal.dBat1mv *= 3300;
			tSenIrriVal.dBat1mv /= 4096;
			tSenIrriVal.dBat1mv *= 11;
		}
		else
		{
			tSenIrriVal.dBat1mv = 0;
		}

		// SOLAR CURRENT
		if (nCountSol1mAmpeSample != 0)
		{
			tSenIrriVal.dSol1mAmpe = dSol1mAmpeSum/nCountSol1mAmpeSample;
			tSenIrriVal.dSol1mAmpe *= 3300;
			tSenIrriVal.dSol1mAmpe /= 4096;
			tSenIrriVal.dSol1mAmpe = (11.2) * tSenIrriVal.dSol1mAmpe - 30000;
		}
		else
		{
			tSenIrriVal.dSol1mAmpe = 0;
		}

		// SOLAR VOLTAGE
		if (nCountSol1mvSample != 0)
		{
			tSenIrriVal.dSol1mv = dSol1mvSum/nCountSol1mvSample;
			tSenIrriVal.dSol1mv *= 3300;
			tSenIrriVal.dSol1mv /= 4096;
			tSenIrriVal.dSol1mv *= 11;
		}
		else
		{
			tSenIrriVal.dSol1mv = 0;
		}


		/* Reset for next period */
		dWT1Sum = 0;
		dWT2Sum = 0;
		dPh1Sum = 0;
		dPh2Sum = 0;
		dEc1Sum = 0;
		dEc2Sum = 0;
		dDo1Sum = 0;
		dDo2Sum = 0;
		dOrp1Sum = 0;
		dOrp2Sum = 0;
		dBat1mAmpeSum = 0;
		dBat1mvSum = 0;
		dSol1mAmpeSum = 0;
		dSol1mvSum = 0;
		nCountIrriSample = 0;
		nCountEc1Sample = 0;
		nCountWt1Sample = 0;
		nCountEc2Sample = 0;
		nCountWt2Sample = 0;
		nCountPh1Sample = 0;
		nCountPh2Sample = 0;
		nCountDo1Sample = 0;
		nCountDo2Sample = 0;
		nCountOrp1Sample = 0;
		nCountOrp2Sample = 0;
		nCountBat1mAmpeSample = 0;
		nCountBat1mvSample = 0;
		nCountSol1mAmpeSample = 0;
		nCountSol1mvSample = 0;
	}

	return true;
}

bool SENSOR_IrriCheck(void)
{
//	double dDeltaEc = fabs(tSensorIrriValue.dEc1 - tSensorIrriValue.dEc2);
//	double dAvgEc = (tSensorIrriValue.dEc1 + tSensorIrriValue.dEc2) / 2;
//	double dDeltaPh = fabs(tSensorIrriValue.dPh1 - tSensorIrriValue.dPh2);
//	double dAvgPh = (tSensorIrriValue.dPh1 + tSensorIrriValue.dPh2) / 2;
//	double dDeltaWt = fabs(tSensorIrriValue.dWaterTemp1 - tSensorIrriValue.dWaterTemp2);
//	double dAvgWt = (tSensorIrriValue.dWaterTemp1 + tSensorIrriValue.dWaterTemp2) / 2;
//
//	if (dDeltaEc >= (double)tSensorThrehold._nEcDual / 100)
//		tSensorIrriValue.tEcCheck = CHECK_EC_DUAL;
//	else if (dAvgEc >= (double)tSensorThrehold._nEc / 100 + 0.2)
//		tSensorIrriValue.tEcCheck = CHECK_EC_HIGH;
//	else if (dAvgEc <= (double)tSensorThrehold._nEcLow / 100)
//		tSensorIrriValue.tEcCheck = CHECK_EC_LOW;
//	else
//		tSensorIrriValue.tEcCheck = CHECK_EC_OK;
//
//	if (dDeltaPh >= (double)tSensorThrehold._nPhDual / 100)
//		tSensorIrriValue.tPhCheck = CHECK_PH_DUAL;
//	else if (dAvgPh >= (double)tSensorThrehold._nPhHigh / 100)
//		tSensorIrriValue.tPhCheck = CHECK_PH_HIGH;
//	else if (dAvgPh <= (double)tSensorThrehold._nPh / 100 - 1)
//		tSensorIrriValue.tPhCheck = CHECK_PH_LOW;
//	else
//		tSensorIrriValue.tPhCheck = CHECK_PH_OK;
//
//	if (dDeltaWt >= 5)
//		tSensorIrriValue.tWtCheck = CHECK_WT_DUAL;
//	else if (dAvgWt >= (tSensorThrehold._nWTemp + 4))
//		tSensorIrriValue.tWtCheck = CHECK_WT_HIGH;
//	else if (dAvgWt <= (tSensorThrehold._nWTemp - 4))
//		tSensorIrriValue.tWtCheck = CHECK_WT_LOW;
//	else
//		tSensorIrriValue.tWtCheck = CHECK_WT_OK;

	return true;
}

/*
Input: stand: standard solution
Output: retEc1: CALIB_EC1_A
Output: retEc2: CALIB_EC2_A
*/
bool SENSOR_CalibEc(uint16_t stand, int16_t *retEc1, int16_t *retEc2)
{
//	double cal_Ec1_A, cal_Ec2_A;
//
//	cal_Ec1_A = (double)(stand * (1 + a * tSensorIrriValue.dWaterTemp1 - a * (T0 - 273.15)));
//	cal_Ec1_A = (double)cal_Ec1_A / 10;
//	cal_Ec1_A = (double)(cal_Ec1_A - tSensorParam.CALIB_EC1_B);
//	cal_Ec1_A = (double)cal_Ec1_A * 1000;
//	cal_Ec1_A = (double)cal_Ec1_A / tSensorIrriValue.dEc1mv;
//	*retEc1 = (int16_t)cal_Ec1_A;
//
//	cal_Ec2_A = (double)(stand * (1 + a * tSensorIrriValue.dWaterTemp2 - a * (T0 - 273.15)));
//	cal_Ec2_A = (double)cal_Ec2_A / 10;
//	cal_Ec2_A = (double)(cal_Ec2_A - tSensorParam.CALIB_EC2_B);
//	cal_Ec2_A = (double)cal_Ec2_A * 1000;
//	cal_Ec2_A = (double)cal_Ec2_A / tSensorIrriValue.dEc2mv;
//	*retEc2 = (int16_t)cal_Ec2_A;

	return true;
}

/*
input: stand : standard solution
output: ph1A, ph1B, ph2A, ph2B
*/
bool SENSOR_CalibPh(uint16_t stand, int16_t *ph1A, int16_t *ph1B, int16_t *ph2A, int16_t *ph2B)
{
//	double cal;
//
//	switch (stand)
//	{
//	case 401:
//		cal = (double)tSensorParam.NEUTRAL_PH1_VOLTAGE - tSensorIrriValue.dPh1mv;
//		cal = (double)(7.01 - 4.01) / cal;
//		cal = (double)cal * 100000;
//		*ph1A = (int16_t)cal;
//		cal = (double)*ph1A / 100000;
//		cal = (double)cal * tSensorIrriValue.dPh1mv;
//		cal = (double)(4.01 - cal);
//		cal = (double)cal * 100;
//		*ph1B = (int16_t)cal;
//
//		cal = (double)tSensorParam.NEUTRAL_PH2_VOLTAGE - tSensorIrriValue.dPh2mv;
//		cal = (double)(7.01 - 4.01) / cal;
//		cal = (double)cal * 100000;
//		*ph2A = (int16_t)cal;
//		cal = (double)*ph2A / 100000;
//		cal = (double)cal * tSensorIrriValue.dPh2mv;
//		cal = (double)(4.01 - cal);
//		cal = (double)cal * 100;
//		*ph2B = (int16_t)cal;
//
//		tSensorParam.ACID_PH1_VOLTAGE = tSensorIrriValue.dPh1mv;
//		tSensorParam.ACID_PH2_VOLTAGE = tSensorIrriValue.dPh2mv;
//		break;
//	case 701:
//		cal = (double)(tSensorIrriValue.dPh1mv - tSensorParam.ACID_PH1_VOLTAGE);
//		cal = (double)(7.01 - 4.01) / cal;
//		cal = (double)(cal * 100000);
//		*ph1A = (int16_t)cal;
//		cal = (double)*ph1A / 100000;
//		cal = (double)cal * tSensorIrriValue.dPh1mv;
//		cal = (double)(7.01 - cal);
//		cal = (double)cal * 100;
//		*ph1B = (int16_t)cal;
//
//		cal = (double)(tSensorIrriValue.dPh2mv - tSensorParam.ACID_PH2_VOLTAGE);
//		cal = (double)(7.01 - 4.01) / cal;
//		cal = (double)(cal * 100000);
//		*ph2A = (int16_t)cal;
//		cal = (double)*ph2A / 100000;
//		cal = (double)cal * tSensorIrriValue.dPh2mv;
//		cal = (double)(7.01 - cal);
//		cal = (double)cal * 100;
//		*ph2B = (int16_t)cal;
//
//		tSensorParam.NEUTRAL_PH1_VOLTAGE = tSensorIrriValue.dPh1mv;
//		tSensorParam.NEUTRAL_PH2_VOLTAGE = tSensorIrriValue.dPh2mv;
//		break;
//	}

	return true;
}

bool SENSOR_CalibWt(volatile float avg)
{
//	double a0 = avg*100;
//	double a1 = tSensorIrriValue.dWaterTemp1*100;
//	double a2 = tSensorIrriValue.dWaterTemp2*100;
//
//	/* calib for wt 1 */
//	if (a0 >= a1)
//	{
//		tSensorParam.OFFSET_CALIB_WT1 += (uint16_t)(a0 - a1);
//	}
//	else
//	{
//		tSensorParam.OFFSET_CALIB_WT1 -= (uint16_t)(a0 - a1);
//	}
//
//	/* calib for wt 2 */
//	if (a0 >= a2)
//	{
//		tSensorParam.OFFSET_CALIB_WT2 += (uint16_t)(a0 - a2);
//	}
//	else
//	{
//		tSensorParam.OFFSET_CALIB_WT2 -= (uint16_t)(a0 - a2);
//	}

	return true;
}
