/*
 * sensor.h
 *
 *  Created on: 2020. 8. 16.
 *      Author: Cuong Le Hung
 */
#ifndef __SENSOR_H
#define __SENSOR_H

#ifdef STM32F407xx
#include "stm32f4xx_hal.h"
#endif
#ifdef STM32F103xx
#include "stm32f1xx_hal.h"
#endif
#include <stdlib.h>
#include <stdbool.h>

//#define VOL_AMP_3V3

typedef enum
{
	CHECK_EC_OK = 1,
	CHECK_EC_HIGH = 2,
	CHECK_EC_LOW = 3,
	CHECK_EC_DUAL = 4
} sysEcCheck_t;

typedef enum
{
	CHECK_WT_OK = 5,
	CHECK_WT_HIGH = 6,
	CHECK_WT_LOW = 7,
	CHECK_WT_DUAL = 8
} sysWtCheck_t;

typedef enum
{
	CHECK_PH_OK = 9,
	CHECK_PH_HIGH = 10,
	CHECK_PH_LOW = 11,
	CHECK_PH_DUAL = 12
} sysPhCheck_t;

typedef struct
{
	double dEc1;
	double dEc1mv;
	double dEc2;
	double dEc2mv;
	double dWT1;
	double dWT1mv;
	double dWT2;
	double dWT2mv;
	double dPh1;
	double dPh1mv;
	double dPh2;
	double dPh2mv;
	double dDo1;
	double dDo1mv;
	double dDo2;
	double dDo2mv;
	double dOrp1;
	double dOrp1mv;
	double dOrp2;
	double dOrp2mv;
	double dBat1mAmpe;
	double dBat1mv;
	double dSol1mAmpe;
	double dSol1mv;
	sysEcCheck_t tEcCheck;
	sysWtCheck_t tWtCheck;
	sysPhCheck_t tPhCheck;
} _SENSOR_IRRI;

void SENSOR_IrriInit(ADC_HandleTypeDef *handle1, ADC_HandleTypeDef *handle2);
void SENSOR_IrriInitSingle(ADC_HandleTypeDef *handle1);
bool SENSOR_IrriRead(void);
bool SENSOR_IrriCheck(void);
bool SENSOR_CalibEc(uint16_t stand, int16_t *retEc1, int16_t *retEc2);
bool SENSOR_CalibPh(uint16_t stand, int16_t *ph1A, int16_t *ph1B, int16_t *ph2A, int16_t *ph2B);
bool SENSOR_CalibWt(volatile float offset);

#endif
