#ifndef _CONFIG_H_
#define _CONFIG_H_

/*
 * FLASH ADDRESS BEGIN
 */
#define INTERNAL_FLASH_START_ADDRESS ((uint32_t)0x0807F800)

/*
 * uncomment this for using device as slave/ comment this for using as master
 */
// #define SPF_SLAVE

/*
 *
 */


#define ON_OFF_AMP

/* COMMAND FROM ST TO ESP32 */
#define CMD_ST_RECONNECT    "S001"
#define CMD_ST_TRANSMIT     "S002"
#define CMD_ST_IS_CONNECT   "S003"

#define CMD_ST_TYPE_IRR     "IRRI"
#define CMD_ST_TYPE_CLI     "ENVI"
#define CMD_ST_TYPE_PARAMS  "PARA"

/* COMMAND FROM ESP32 TO ST */
#define CMD_WIFI_NOT_CONNECTED      "E000"
#define CMD_WIFI_CONNECTED          "E001"
#define CMD_WIFI_CONNECT_FAIL       "E003"
#define CMD_WIFI_RECONNECTING       "E004"
#define CMD_WIFI_RESPONSE_OK        "E005"
#define CMD_WIFI_RESPONSE_OTA       "E006"

//KIEN DEFINE
#define CMD_WIFI_CONTROLPUMP  "E007"
//KIEN

#endif
