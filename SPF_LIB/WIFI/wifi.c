/*
 * wifi.c
 *
 *  Created on: 2020. 9. 25.
 *      Author: 
 */
#include "../include.h"

extern _DEV_INFO tDevInfo;
extern _SENSOR_IRRI tSenIrriVal;
static UART_HandleTypeDef *uart;

/* Private structure */
WifiState_t WifiState;
_WIFI WifiInfo = {
    .nIpAddress = "0.0.0.0",
    .bState = false,
    .nRssi = 80};

/* Private variables */
uint16_t nTimePeriod = 0;
uint16_t nTimeWifiCheck = 0;
int8_t nTimeOut = 0;
int16_t nTransmitTimerCount = 0;
int16_t nSysInfoTimerCount = 0;
char cmdBuf[LEN_CMD];
char str[321];
char cmd[4];
char data[LEN_CMD - 4];

/* Local function */
static bool Cmdcompare(char *s, char *d);
static char *subString(char *s, int pos, int index);
static bool sendCmd(char *str);
static bool transmitSysInfo(void);

/* Function definition */

void WIFI_Init(UART_HandleTypeDef *handle)
{
    uart = handle;
    HAL_UART_Receive_DMA(uart, (uint8_t *)cmdBuf, LEN_CMD);
    WifiState = WIFI_CHECK;
}

bool WIFI_Process(void)
{
    char *a = subString(cmdBuf, 0, 4);
    char *b = subString(cmdBuf, 5, LEN_CMD - 5);

    if ((a[0] == 'E') && (a[1] == '0'))
    {
        sprintf(cmd, "%s", a);
        sprintf(data, "%s", b);
    }

    switch (WifiState)
    {
    case WIFI_RECONNECT:
        if (Cmdcompare(cmd, CMD_WIFI_RECONNECTING))
        {
            WifiState = WIFI_CHECK;
        }
        else
        {
            sendCmd(CMD_ST_RECONNECT);
        }
        break;
    case WIFI_CHECK:
        if (nTimeWifiCheck == 0)
        {
            nTimeWifiCheck = 10;
            memset(cmdBuf, '\0', (size_t)LEN_CMD);
            sendCmd(CMD_ST_IS_CONNECT);
        }
        else
        {
            if (Cmdcompare(cmd, CMD_WIFI_CONNECTED))
            {
                WifiInfo.bState = true;
                sprintf(WifiInfo.nIpAddress, "%s", data);
                // memset(cmdBuf, '\0', (size_t)LEN_CMD);

                if (nTimePeriod == 0)
                {
                    nTimePeriod = tDevInfo.nTimeUpload;
                    if (nSysInfoTimerCount == 0)
                    {
                        WifiState = WIFI_TRANS_SYSINFO;
                    }
                    else
                    {
                        WifiState = WIFI_TRANS_IRRI;
                    }
                }
            }
            else
            {
                WifiInfo.bState = false;
                sprintf(WifiInfo.nIpAddress, "%s", "Invalid");
            }
        }
        break;
    case WIFI_TRANS_SYSINFO:
        transmitSysInfo();
        nSysInfoTimerCount = 21600; //6h
        nTransmitTimerCount = 2;
        WifiState = WIFI_TRANS_IRRI;
        break;
    case WIFI_TRANS_IRRI:
        if (nTransmitTimerCount == 0)
        {
            transmitIrri();
            WifiState = WIFI_CHECK;
        }
        break;
    }

    HAL_UART_AbortReceive(uart);
    HAL_UART_Receive_DMA(uart, (uint8_t *)cmdBuf, LEN_CMD);

    return true;
}

bool WIFI_Timer1000ms(void)
{
    if (nTimeOut > 0)
        nTimeOut--;
    if (nSysInfoTimerCount > 0)
        nSysInfoTimerCount--;
    if (nTransmitTimerCount > 0)
        nTransmitTimerCount--;
    if (nTimeWifiCheck > 0)
        nTimeWifiCheck--;
    if (nTimePeriod > 0)
        nTimePeriod--;

    return true;
}

bool sendCmd(char *str)
{
//    if (nTimeOut == 0)
//    {
        
        HAL_UART_Transmit(uart, (uint8_t *)str, strlen(str), 1000);
//        nTimeOut = 5;
//    }

    return true;
}

bool Cmdcompare(char *s, char *d)
{
    if ((s[0] == d[0]) && (s[1] == d[1]) && (s[2] == d[2]) && (s[3] == d[3]))
    {
        return true;
    }
    else
    {
        return false;
    }
}

char *subString(char *s, int pos, int index)
{
    char *t = &s[pos];
    s[pos - 1] = '\0';
    for (int i = index; i < (strlen(t) + 1); i++)
    {
        t[i] = '\0';
    }
    return t;
}

bool transmitSysInfo(void)
{
    return true;
}

bool transmitIrri(void)
{
    char data[100];

    sprintf(str, "%s,%s,pCode=TOM", CMD_ST_TRANSMIT, CMD_ST_TYPE_IRR);
    sprintf(data, "%03d", (int)tDevInfo.nMasterCode); // Master Code
    strcat(str, data);
    sprintf(data, "%03d", 1); // Slave Code
    strcat(str, data);
    sprintf(data, "&pEC1=%01.02f&pEC2=%01.02f&pECs=OK", tSenIrriVal.dEc1, tSenIrriVal.dEc2);
    strcat(str, data);
    sprintf(data, "&pPH1=%01.02f&pPH2=%01.02f&pPHs=OK", tSenIrriVal.dPh1, tSenIrriVal.dPh2);
    strcat(str, data);
    sprintf(data, "&pWT1=%02.02f&pWT2=%02.02f&pWTs=OK", tSenIrriVal.dWT1, tSenIrriVal.dWT2);
    strcat(str, data);
    sprintf(data, "&pORP1=%04f&pORP2=%04f&pORPs=OK", tSenIrriVal.dOrp1, tSenIrriVal.dOrp2);
    strcat(str, data);
    sprintf(data, "&pDO1=%02.02f&pDO2=%02.02f&pDOs=OK", tSenIrriVal.dDo1, tSenIrriVal.dDo2);
    strcat(str, data);
    sprintf(data, "&pBattV1=%02.01f&pBattV2=%02.01f&pBattVs=OK", tSenIrriVal.dBat1mv / 1000, 0.0);
    strcat(str, data);
    sprintf(data, "&pBattC1=%01.02f&pBattC2=%01.02f&pBattCs=OK", tSenIrriVal.dBat1mAmpe / 1000, 0.0);
    strcat(str, data);
    sprintf(data, "&pSolV1=%02.01f&pSolV2=%02.01f&pSolVs=OK", tSenIrriVal.dSol1mv / 1000, 0.0);
    strcat(str, data);
    //    sprintf(data, "&pSolC1=%01.02f&pSolC2=%01.02f", tSenIrriVal.dSol1mAmpe, 0);
    //    strcat(str, data);

    if (HAL_UART_Transmit(uart, (uint8_t *)str, strlen(str), 1000) == HAL_OK)
    {
        return true;
    }
    else
    {
        return false;
    }
}
