/*
 * wifi.h
 *
 *  Created on: 2020. 9. 25.
 *      Author: 
 */
#ifndef __WIFI_H
#define __WIFI_H

#ifdef STM32F407xx
#include "stm32f4xx_hal.h"
#endif
#ifdef STM32F103xx
#include "stm32f1xx_hal.h"
#endif
#include <stdlib.h>
#include <stdbool.h>

#define LEN_CMD 21

typedef enum
{
    WIFI_CHECK = 10,
    WIFI_RECONNECT,
    WIFI_TRANS_SYSINFO,
    WIFI_TRANS_IRRI,
} WifiState_t;

typedef struct
{
    char nIpAddress[LEN_CMD - 4];
    bool bState;
    uint8_t nRssi;
} _WIFI;

//-----Function prototype
void WIFI_Init(UART_HandleTypeDef *handle);
bool WIFI_Process(void);
bool WIFI_Timer1000ms(void);
bool transmitIrri(void);

#endif
